<!DOCTYPE html>
<!--[if lt IE 10]> <html class="ie-lt-10"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<html lang="en-US">

<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	
	<title>
		     Page not found - Dutco Styles&amp;Wood	</title>
		<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' id='font-awesome-css'  href='/sw-cms/wp-content/themes/craper/css/fonts/font-awesome.css' type='text/css' media='all' />
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
    		<link rel="shortcut icon" href="http://www.dutcosw.com/sw-cms/wp-content/uploads/Dutco_StylesWood_Arabic_logo1.jpg" type="image/x-icon" />
		
		<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<link rel="pingback" href="http://www.dutcosw.com/sw-cms/xmlrpc.php" />

	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50525861-3', 'auto');
  ga('send', 'pageview');

</script>


	<link rel="alternate" type="application/rss+xml" title="Dutco Styles&amp;Wood &raquo; Feed" href="http://www.dutcosw.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dutco Styles&amp;Wood &raquo; Comments Feed" href="http://www.dutcosw.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.dutcosw.com\/sw-cms\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.3"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='maplistCoreStyleSheets-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/MapListPro/css/MapListProCore.css?ver=3.5.5' type='text/css' media='all' />
<link rel='stylesheet' id='maplistStyleSheets-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/MapListPro/styles/Grey_light_default.css?ver=3.5.5' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/LayerSlider/static/css/layerslider.css?ver=5.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900|Open+Sans:300|Indie+Flower:regular|Oswald:300,regular,700&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.3.8&#038;ver=4.5.3' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
.tp-caption a {
color:#ff7302;
text-shadow:none;
-webkit-transition:all 0.2s ease-out;
-moz-transition:all 0.2s ease-out;
-o-transition:all 0.2s ease-out;
-ms-transition:all 0.2s ease-out;
}

.tp-caption a:hover {
color:#ffa902;
}
</style>
<link rel='stylesheet' id='rs-plugin-captions-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/revslider/rs-plugin/css/captions.php?rev=4.3.8&#038;ver=4.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='showbiz-settings-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/showbiz/showbiz-plugin/css/settings.css?rev=1.5.1&#038;ver=4.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='fancybox-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/showbiz/showbiz-plugin/fancybox/jquery.fancybox.css?rev=1.5.1&#038;ver=4.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=4.11.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='font-typicons-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/fonts/typicons.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='ico-font-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/fonts/ico-font.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='revolution-slider-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/revolution-slider/settings.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/bootstrap.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/style.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='color-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/color.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='animate-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/animate.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='prettyPhoto-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/prettyPhoto.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='custome-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/custome.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/css/responsive.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='main_style-css'  href='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/style.css?ver=4.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='//www.dutcosw.com/sw-cms/wp-content/uploads/js_composer/js_composer_front_custom.css?ver=4.11.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='vc_plugin_template_theme_css-css'  href='http://www.dutcosw.com/sw-cms/wp-content/plugins/templatera/assets/css/themes/%20?ver=templatera_1' type='text/css' media='all' />
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.plugins.min.js?rev=4.3.8&#038;ver=4.5.3'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.3.8&#038;ver=4.5.3'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/plugins/showbiz/showbiz-plugin/fancybox/jquery.fancybox.pack.js?rev=1.5.1&#038;ver=4.5.3'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/plugins/showbiz/showbiz-plugin/js/jquery.themepunch.showbizpro.min.js?rev=1.5.1&#038;ver=4.5.3'></script>
<link rel='https://api.w.org/' href='http://www.dutcosw.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.dutcosw.com/sw-cms/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.dutcosw.com/sw-cms/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.5.3" />
<script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "http://www.dutcosw.com/sw-cms/wp-admin/admin-ajax.php";</script>		<style type="text/css">
					</style>
        
                <script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "http://www.dutcosw.com/sw-cms/wp-admin/admin-ajax.php";</script>		<style type="text/css">
					</style>
        
                <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://www.dutcosw.com/sw-cms/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://www.dutcosw.com/sw-cms/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>

<body class="error404 wpb-js-composer js-comp-ver-4.11.2.1 vc_responsive vct_ ">
	
	<div class="pageWrapper">
	<div class="top-strip">
        <div class="container">
            <div class="pull-left">
                <ul class="inline">
                
                	                    
                                    </ul>
            </div>
            <div class="pull-right">
                <ul class="social-links">
                    <li><a title="DutcoStyles&Wood" class="icon-fa icon-twitter" target="_blank" href="https://twitter.com/DutcoStylesWood"><span class="icon-fa icon-twitter"></span></a></li>
<li><a title="Linkedin" class="icon-fa icon-linkedin" target="_blank" href="https://www.linkedin.com/company/1936598"><span class="icon-fa icon-linkedin"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
	<div class="logo-bar">
        <div class="container"> 
            
            <!-- Logo -->
            <div class="logo"> 
                
                <a href="http://www.dutcosw.com">
                                            <img alt="Logo" src="http://www.dutcosw.com/sw-cms/wp-content/uploads/Dutco_StylesWood_Arabic_logo1.jpg" />
                                    </a> 
                
            </div>
            <div class="pull-right"> <span class="nav-button"></span>
                <nav class="main-nav">
                    <ul id="menu-new" class="menu"><li id="menu-item-2106" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2106"><a title="HOME" href="http://www.dutcosw.com/" data-dup="HOME"><span class="glyphicon Home"></span>&nbsp;HOME</a></li>
<li id="menu-item-4151" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4151"><a title="ABOUT US" href="http://www.dutcosw.com/aboutus/" data-dup="ABOUT US">ABOUT US</a></li>
<li id="menu-item-4202" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4202"><a title="SERVICES" href="http://www.dutcosw.com/ourservices/" data-dup="SERVICES">SERVICES</a></li>
<li id="menu-item-4238" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4238"><a title="SECTORS" href="http://www.dutcosw.com/oursectors/" data-dup="SECTORS">SECTORS</a></li>
<li id="menu-item-4605" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4605"><a title="CASE STUDIES" href="http://www.dutcosw.com/case-studies/" data-dup="CASE STUDIES">CASE STUDIES</a></li>
<li id="menu-item-4357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4357"><a title="CAREERS" href="http://www.dutcosw.com/careers/" data-dup="CAREERS">CAREERS</a></li>
<li id="menu-item-4528" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-4528"><a title="LATEST" href="http://www.dutcosw.com/latest/" data-dup="LATEST">LATEST</a></li>
<li id="menu-item-4258" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4258"><a title="CONTACT US" href="http://www.dutcosw.com/contact/" data-dup="CONTACT US">CONTACT US</a></li>
</ul>                </nav>
            </div>
        </div>
    </div>
<article class="banner">
	<div class="container">
		<h2>  Page not found</h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
				<li><a href="http://www.dutcosw.com">Home &nbsp;</a></li><li>404 - Not Found</li>			</ul>
		</div>
	</div>
</article>

<article role="main">
	<div class="section">
		<div class="container">
        
        	<div class="row">
			
                <div class="error-box col-xs-12 col-sm-8 col-md-12"> 
                                            <img src="http://www.dutcosw.com/sw-cms/wp-content/themes/craper/images/error.png" alt="error">
                                        
                    <p>
                                                    <h2>404 Not Found</h2>
                                                
                                                    <strong>Uuuuuuups</strong>, we are sorry but this page does not exist.                                            </p>
                    
                                            <a href="http://www.dutcosw.com" class="btn btn-primary btn-icon-hold"><i class="icon-page-arrow-right "></i><span>Go Back to Home page</span></a> 
                                        
                </div>
                
                            </div>
		</div>
	</div>
</article>



    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.tweets-shortcode').tweets({"template":"blockquote","count":3,"screen_name":"wowtheme"});
        });
    </script>


<article class="focus-area bg-theme">
			<div class="container">
				<div class="tweets-shortcode"></div>
			</div>
		</article>	

 <footer>
        <div class="container">
            <div class="row">
			
			<div class="col-md-4">
			<h3>Styles&amp;Wood Group Links</h3>			
				<p><a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br />
				<a href="http://www.stylesandwood-group.co.uk/sitemap/">Sitemap</a><br />
				<a href="http://www.stylesandwood-group.co.uk/contact/">Contact Us</a></p>
			</div>
	
			
		<div class="col-md-4">
		<h3>Current Share Price</h3>		
			 <p>312.50 GBp</p>
		</div>
		</div>
        
		<div class="row">
				<div class="span12">
					<div class="bottom fixclear">
					
					<ul class="social-icons colored fixclear"><li class="title">See more from the Styles&amp;Wood Group</li><li class="social-twitter"><a target="_blank" href="https://twitter.com/StylesandWood">Twitter</a></li><li class="social-linkedin"><a target="_blank" href="http://www.linkedin.com/company/78033">LinkedIn</a></li></ul>					
											
						<div class="copyright">
							
							
							<a href="http://www.stylesandwood-group.co.uk"><img alt="Styles&amp;Wood Group PLC" src="http://stylesandwood-group.co.uk/sw-cms/wp-content/uploads/SylesWood-sm.png"></a><p>&copy; 2016 Copyright - Styles&amp;Wood Group PLC</p>							
							
						</div><!-- end copyright -->
							
											

					</div><!-- end bottom -->
				</div>
			</div>
		
		</div>
    </footer>
</div>
<!-- /pageWrapper -->

<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/jquery/ui/tooltip.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.appear.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.carouFredSel-6.2.1-packed.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.knob.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.mixitup.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.prettyPhoto.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.roundabout.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery.roundabout-shapes.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/jquery-ui-1.10.3.custom.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/masonry.pkgd.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/script.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/addthis.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-content/themes/craper/js/addthis-script.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw-cms/wp-includes/js/wp-embed.min.js?ver=4.5.3'></script>

<!-- Don't forget analytics -->
	
</body>

</html>
