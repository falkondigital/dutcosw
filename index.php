<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */
ini_set('allow_url_fopen ','ON');
ini_set('memory_limit', '256M');
/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
//if( $_SERVER['REMOTE_ADDR'] == "81.137.198.213"  ){   
//	require('./sw-cms/wp-blog-header.php');
//}else{
	require('./sw-cms/wp-blog-header.php');
//}
