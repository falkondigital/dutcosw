<!DOCTYPE html>

<html lang="en-US" class="no-js" >

<head>
 	<title>
    Dutco | Dutco Styles&amp;Wood &#8211; News    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="http://www.dutcosw.com/sw/xmlrpc.php" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50525861-3', 'dutcosw.com');
  ga('send', 'pageview');

</script>
	<link rel="alternate" type="application/rss+xml" title="Dutco &raquo; Feed" href="http://www.dutcosw.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dutco &raquo; Comments Feed" href="http://www.dutcosw.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dutco &raquo; Dutco Styles&amp;Wood &#8211; News Comments Feed" href="http://www.dutcosw.com/news/feed/" />
<link rel='stylesheet' id='jetpack-subscriptions-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/jetpack/modules/subscriptions/subscriptions.css' type='text/css' media='all' />
<link rel='stylesheet' id='maplistCoreStyleSheets-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/MapListPro/css/MapListProCore.css' type='text/css' media='all' />
<link rel='stylesheet' id='maplistStyleSheets-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/MapListPro/styles/Grey_light_default.css' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider_css-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/LayerSlider/css/layerslider.css' type='text/css' media='all' />
<link rel='stylesheet' id='showbiz-settings-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/showbiz/showbiz-plugin/css/settings.css' type='text/css' media='all' />
<link rel='stylesheet' id='fancybox-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/showbiz/showbiz-plugin/fancybox/jquery.fancybox.css' type='text/css' media='all' />
<link rel='stylesheet' id='ubermenu-basic-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/styles/basic.css' type='text/css' media='all' />
<link rel='stylesheet' id='ubermenu-silver-tabs-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/styles/skins/silvertabs.css' type='text/css' media='all' />
<link rel='stylesheet' id='jetpack-widgets-css'  href='http://www.dutcosw.com/sw/wp-content/plugins/jetpack/modules/widgets/widgets.css' type='text/css' media='all' />
<link rel='stylesheet' id='zn-bootstrapcss-css'  href='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/css/bootstrap.css' type='text/css' media='all' />
<link rel='stylesheet' id='zn-superfish-css'  href='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/addons/superfish_responsive/superfish.css' type='text/css' media='all' />
<link rel='stylesheet' id='zn-templtecss-css'  href='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/css/template.css' type='text/css' media='all' />
<link rel='stylesheet' id='zn-bootstrap-responsivecss-css'  href='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/css/bootstrap-responsive.css' type='text/css' media='all' />
<link rel='stylesheet' id='pretty_photo-css'  href='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/addons/prettyphoto/prettyPhoto.css' type='text/css' media='all' />
<link rel='stylesheet' id='theme_style-css'  href='http://www.dutcosw.com/sw/wp-content/themes/Custom/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='Lato_default-css'  href='//fonts.googleapis.com/css?family=Lato%3A300%2C400%2C700%2C900&#038;v1&#038;subset=latin%2Clatin-ext' type='text/css' media='screen' />
<link rel='stylesheet' id='Open+Sans_default-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C700&#038;v1&#038;subset=latin%2Clatin-ext' type='text/css' media='screen' />
<link rel='stylesheet' id='zn_all_g_fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3Aregular%7CLato%3Aregular%7CNunito%3Aregular%7CRaleway%3Aregular' type='text/css' media='all' />
<link rel='stylesheet' id='options-css'  href='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/css/options.css' type='text/css' media='all' />
<script>var jquery_placeholder_url = 'http://www.dutcosw.com/sw/wp-content/plugins/gravity-forms-placeholders/jquery.placeholder-1.0.1.js';</script><script type='text/javascript' src='http://www.dutcosw.com/sw/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/LayerSlider/js/layerslider.kreaturamedia.jquery.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/LayerSlider/js/jquery-easing-1.3.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/LayerSlider/js/jquerytransit.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/LayerSlider/js/layerslider.transitions.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/showbiz/showbiz-plugin/fancybox/jquery.fancybox.pack.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/showbiz/showbiz-plugin/js/jquery.themepunch.showbizpro.min.js'></script>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/gravity-forms-placeholders/gf.placeholders.js'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.dutcosw.com/sw/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.dutcosw.com/sw/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Projects' href='http://www.dutcosw.com/projects/' />
<link rel='next' title='Careers' href='http://www.dutcosw.com/careers/' />
<meta name="generator" content="WordPress 3.9.1" />
<link rel='canonical' href='http://www.dutcosw.com/news/' />
<link rel='shortlink' href='http://wp.me/P3Du8s-20' />
<link rel="shortcut icon" href="http://www.dutcosw.com/sw/wp-content/uploads/dutco.png"/>			<!--[if lte IE 9]>
				<link rel="stylesheet" type="text/css" href="http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/css/fixes.css" />
			<![endif]-->

			
			<!--[if lte IE 8]>
				<script src="http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/js/respond.js"></script>
				<script type="text/javascript">
					var $buoop = {
						vs: {
							i: 8,
							f: 6,
							o: 10.6,
							s: 4,
							n: 9
						}
					}
					$buoop.ol = window.onload;
					window.onload = function () {
						try {
							if ($buoop.ol) $buoop.ol();
						} catch (e) {}
						var e = document.createElement("script");
						e.setAttribute("type", "text/javascript");
						e.setAttribute("src", "http://browser-update.org/update.js");
						document.body.appendChild(e);
					}
				</script>
			<![endif]-->
			
									<!-- Facebook OpenGraph Tags - Replace with your own -->
						<meta property="og:title" content="Dutco Styles&#038;Wood &#8211; News"/>
						<meta property="og:type" content="article"/>
						<meta property="og:url" content="http://www.dutcosw.com/news/"/>
												<meta property="og:site_name" content=" Dutco"/>
						<meta property="fb:app_id" content=""/> <!-- PUT HERE YOUR OWN APP ID - you could get errors if you don't use this one -->
						<meta property="og:description" content=" "/>
						<!-- END Facebook OpenGraph Tags -->
					
			<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
			<!--[if lt IE 9]>
				<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /><meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>

<!-- Jetpack Open Graph Tags -->
<meta property="og:type" content="article" />
<meta property="og:title" content="Dutco Styles&amp;Wood - News" />
<meta property="og:url" content="http://www.dutcosw.com/news/" />
<meta property="og:description" content="Dutco Styles&amp;Wood - News Various news, articles and information about Dutco Styles&amp;Wood and the  Styles&amp;Wood Group Plc" />
<meta property="article:published_time" content="2013-06-25T19:13:18+00:00" />
<meta property="article:modified_time" content="2014-07-01T13:28:21+00:00" />
<meta property="article:author" content="http://www.dutcosw.com/author/msfirth/" />
<meta property="og:site_name" content="Dutco" />
<meta property="og:image" content="http://wordpress.com/i/blank.jpg" />
<meta name="twitter:site" content="@jetpack" />
<meta name="twitter:card" content="summary" />

<!-- UberMenu CSS - Controlled through UberMenu Options Panel 
================================================================ -->
<style type="text/css" id="ubermenu-style-generator-css">
/* Image Text Padding */
#megaMenu .ss-nav-menu-with-img > a > .wpmega-link-title, #megaMenu .ss-nav-menu-with-img > a > .wpmega-link-description, #megaMenu .ss-nav-menu-with-img > a > .wpmega-item-description, #megaMenu .ss-nav-menu-with-img > span.um-anchoremulator > .wpmega-link-title, #megaMenu .ss-nav-menu-with-img > span.um-anchoremulator > .wpmega-link-description, #megaMenu .ss-nav-menu-with-img > span.um-anchoremulator > .wpmega-item-description{
  padding-left: 97px;
}


/**** UberMenu Sticky CSS ****/
#megaMenu.ubermenu-sticky{ margin: 0 !important; z-index:1000; position:fixed !important; top: 0px; bottom: auto !important; -webkit-transition:none; -moz-transition:none; transition:none; }
#megaMenu ul.megaMenu li.um-sticky-only{ display: none !important; }#megaMenu-sticky-wrapper #megaMenu.ubermenu-sticky li.um-sticky-only{ display: block !important; }#megaMenu ul.megaMenu li.um-unsticky-only{ display: block !important; }#megaMenu-sticky-wrapper #megaMenu.ubermenu-sticky li.um-unsticky-only{ display: none !important; }
/* Expand Menu Bar */ 
#megaMenu.ubermenu-sticky{ left: 0; right:auto; width: 100%; border-radius: 0; }
/* Center Inner Menu */ 
#megaMenu.ubermenu-sticky ul.megaMenu{ padding-left:2px; margin: 0 auto; float:none; max-width: 1170px; }
#megaMenu.megaMenuHorizontal ul.megaMenu > li:first-child > a{ box-shadow:none; }	
</style>
<!-- end UberMenu CSS -->
		
					<link rel="stylesheet" id="custom-css-css" type="text/css" href="http://www.dutcosw.com/?custom-css=1&#038;csblog=1&#038;cscache=6&#038;csrev=216" />
		</head>
<body  class="page page-id-124 page-template-default res1170 wpb-js-composer js-comp-ver-3.7.4 vc_responsive">

	<!-- AFTER BODY ACTION -->
				<!-- ADD AN APPLICATION ID !! If you want to know how to find out your
			app id, either search on google for: facebook appid, either go to http://rieglerova.net/how-to-get-a-facebook-app-id/
			-->
			<div id="fb-root"></div>
			<script>
				(function (d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId="; // addyour appId here
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
		

    <div id="page_wrapper">
        <header id="header" class="style3 ">
            <div class="container">
                <!-- logo -->
                <h3 id="logo"><a href="http://www.dutcosw.com"><img src="http://dutcosw.com/sw/wp-content/uploads/Dutco-StylesWood-Logo-XXS.jpg" alt="Dutco" title="" /></a></h3>								
				<!-- HEADER ACTION -->
				               
				<!-- search -->
				
				<!-- main menu -->
				<nav id="main_menu" class="">
										<nav id="megaMenu" class="megaMenuContainer megaMenu-nojs wpmega-preset-silver-tabs megaResponsive megaResponsiveToggle wpmega-withjs megaMenuOnHover megaFullWidthSubs megaMenuHorizontal wpmega-noconflict megaMinimizeResiduals megaResetStyles"><div id="megaMenuToggle" class="megaMenuToggle">Mobile Menu&nbsp; <span class="megaMenuToggle-icon"></span></div><ul id="megaUber" class="megaMenu"><li id="menu-item-112" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-0 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-center"><a href="http://www.dutcosw.com/"><span class="wpmega-link-title">Home</span></a></li><li id="menu-item-1896" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-1 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth mega-colgroup mega-colgroup-6 ss-nav-menu-mega-alignLeft"><a href="http://www.dutcosw.com/about/"><span class="wpmega-link-title">About</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-330" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/about/styles-wood-group-plc/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Our-Business-Model.png&amp;w=90&amp;h=90&amp;zc=1" alt="Our Business Model" title="Our Business Model" class="um-img um-img-timthumb"/></a></li><li id="menu-item-2206" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a title="Our People" href="http://www.dutcosw.com/about/our-people/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Our-People.png&amp;w=90&amp;h=90&amp;zc=1" alt="Our People" title="Our People" class="um-img um-img-timthumb"/></a></li><li id="menu-item-328" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/about/mission-vision-and-values/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/mission-vision-and-values.png&amp;w=90&amp;h=90&amp;zc=1" alt="Vision, Mission and Values" title="Vision, Mission and Values" class="um-img um-img-timthumb"/></a></li><li id="menu-item-1897" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/about/dutco-group/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Dutco.png&amp;w=90&amp;h=90&amp;zc=1" alt="Dutco" title="Dutco" class="um-img um-img-timthumb"/></a></li><li id="menu-item-329" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/about/sheq/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Health-and-Safety.png&amp;w=90&amp;h=90&amp;zc=1" alt="Health and Safety" title="Health and Safety" class="um-img um-img-timthumb"/></a></li></ul>
</li><li id="menu-item-109" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-2 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth mega-colgroup mega-colgroup-6 ss-nav-menu-mega-alignLeft"><a href="http://www.dutcosw.com/services/"><span class="wpmega-link-title">Services</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-1796" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/services/excellence-in-fit-out/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Excellence-in-Fit-out.png&amp;w=90&amp;h=90&amp;zc=1" alt="Excellence in Fit out" title="Excellence in Fit out" class="um-img um-img-timthumb"/></a></li><li id="menu-item-1797" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a title="Experts in Design and Build" href="http://www.dutcosw.com/services/experts-in-design-and-build/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/DesignandBuild.png&amp;w=90&amp;h=90&amp;zc=1" alt="Experts in Design and Build" title="Experts in Design and Build" class="um-img um-img-timthumb"/></a></li><li id="menu-item-1802" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/services/delivering-quality/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Delivering-Quality.png&amp;w=90&amp;h=90&amp;zc=1" alt="Delivering Quality" title="Delivering Quality" class="um-img um-img-timthumb"/></a></li></ul>
</li><li id="menu-item-116" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-3 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth mega-colgroup mega-colgroup-6 ss-nav-menu-mega-alignLeft"><a href="http://www.dutcosw.com/sectors/"><span class="wpmega-link-title">Sectors</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-1813" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/sectors/commercial/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Commercial1.png&amp;w=90&amp;h=90&amp;zc=1" alt="Commercial" title="Commercial" class="um-img um-img-timthumb"/></a></li><li id="menu-item-1874" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/sectors/hospitality/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Hospitality-and-Leisure1.png&amp;w=90&amp;h=90&amp;zc=1" alt="Hospitality" title="Hospitality" class="um-img um-img-timthumb"/></a></li><li id="menu-item-1875" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a title="Leisure" href="http://www.dutcosw.com/sectors/leisure/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Dutco-Leisure1.png&amp;w=90&amp;h=90&amp;zc=1" alt="Leisure" title="Leisure" class="um-img um-img-timthumb"/></a></li><li id="menu-item-1816" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/sectors/retail/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Retail1.png&amp;w=90&amp;h=90&amp;zc=1" alt="Retail" title="Retail" class="um-img um-img-timthumb"/></a></li></ul>
</li><li id="menu-item-130" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-4 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-left"><a href="http://www.dutcosw.com/projects/"><span class="wpmega-link-title">Projects</span></a></li><li id="menu-item-128" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-5 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth mega-colgroup mega-colgroup-6 ss-nav-menu-mega-alignLeft"><a href="http://www.dutcosw.com/careers/"><span class="wpmega-link-title">Careers</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-1901" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-with-img ss-nav-menu-notext"><a href="http://www.dutcosw.com/careers/supply-chain/"><img src="http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/standard/timthumb/tt.php?src=http://www.dutcosw.com/sw/wp-content/uploads/Supply-Chain.png&amp;w=90&amp;h=90&amp;zc=1" alt="Supply Chain" title="Supply Chain" class="um-img um-img-timthumb"/></a></li></ul>
</li><li id="menu-item-129" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-124 current_page_item ss-nav-menu-item-6 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-center active"><a href="http://www.dutcosw.com/news/"><span class="wpmega-link-title">News</span></a></li><li id="menu-item-107" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-7 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-center"><a href="http://www.dutcosw.com/contact/"><span class="wpmega-link-title">Contact</span></a></li><li id="menu-item-1950" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mega-with-sub ss-nav-menu-item-8 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-right ss-nav-menu-mega-floatRight"><a href="#"><span class="wpmega-link-title">Search</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-1952" class="menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-1 ss-nav-menu-highlight ss-nav-menu-notext ss-nav-menu-nolink ss-sidebar"><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-1 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar"><li id="search-4" class="widget widget_search">			<div class="search">				<form id="searchform" action="http://www.dutcosw.com/" method="get">					<input id="s" name="s" maxlength="20" class="inputbox" type="text" size="20" value="SEARCH ..." onblur="if (this.value=='') this.value='SEARCH ...';" onfocus="if (this.value=='SEARCH ...') this.value='';" />					<input type="submit" id="searchsubmit" value="go" class="icon-search"/>				</form>			</div></li>
</ul></div></li></ul>
</li></ul></nav>				</nav>
				<!-- end main_menu -->
			</div>
		</header>
		

			<div id="page_header" class="zn_def_header_style">
				<div class="bgback"></div>

				
					<!-- DEFAULT HEADER STYLE -->
					<div class="container">
						<div class="row">
							<div class="span6">
							 
															</div>
							<div class="span6">
								<div class="header-titles">
																	</div>
							</div>
						</div><!-- end row -->
					</div>
				<div class="zn_header_bottom_style"></div>
			</div><!-- end page_header -->

<section id="content"><div class="container"><div class="mainbody "><div class="row "><div class="span12"><div class="infoBox">
<div class="textBox">
<h1>Dutco Styles&amp;Wood &#8211; News</h1>
<p>Various news, articles and information about Dutco Styles&amp;Wood and the  Styles&amp;Wood Group Plc</p>
				
			<!-- START SHOWBIZ 1.3.1 -->	
			
						
						<style type="text/css">
				/******************************************

	STYLE SETTINGS FOR RETRO DARK SKIN

*******************************************/

#showbiz_news_1 									{ overflow:visible; position:relative;  background:#252525;padding:25px}
#showbiz_news_1 .showbiz-title,
#showbiz_news_1 .showbiz-title a,
#showbiz_news_1 .showbiz-title a:visited,
#showbiz_news_1 .showbiz-title a:hover				{	text-decoration: none;
												font-weight:400 !important;
										        font-size: 16px;
										        font-weight: normal;
										        color: #ddd !important;
										        text-shadow: #000 1px 1px 1px;
										        margin: 0px;
										        width: 100%;
										        text-transform:none !important;
										        text-decoration: none !important;
										    }

#showbiz_news_1 .showbiz-description				{	font-family: Arial, sans-serif;
										        font-size: 12px;
										        line-height: 20px;
										        color: #999;
										        text-shadow: #000 1px 1px 1px;
										     }

#showbiz_news_1 .showbiz-button               		{	padding:4px 15px !important;
                                                color: #999;
												text-shadow: 1px 1px 0px #000;
												background: #333;
												border: 1px solid #151515;
												text-decoration: none;
												font-weight: 400;
                                                white-space: nowrap;
                                                font-size:12px;
                                                text-decoration: none !important;
                                             }

#showbiz_news_1  .showbiz-navigation i				{	text-shadow:0px 1px 0px rgba(0,0,0,0.4); font-size:20px;}

#showbiz_news_1  .mediaholder 						{	background-color:#fff;
	                                            border:1px solid #111;
	                                            border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px;
	                                            padding:0px;
	                                        }

#showbiz_news_1 .hovercover						{	background:rgba(0,0,0,0.5);  }

#showbiz_news_1 li:hover .mediaholder img  		{	 filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale");
                                           		 filter: gray; /* IE6-9 */
                                           		 -webkit-filter: grayscale(100%);
                                           	}


#showbiz_news_1	.linkicon						{	 position:absolute; left:50%;top:50%;
	                                            -webkit-transform: translateZ(10);
	                                            -webkit-backface-visibility: hidden;
	                                            -webkit-perspective: 1000;
	                                            color:#fff; font-size:42px; font-weight:400 !important;

	                                            margin-top:-23px; margin-left:-23px;
	                                             background:#000; background:rgba(0,0,0,0.6);
	                                             border-radius:50%;-moz-border-radius:50%;-webkit-border-radius:50%;

	                                            vertical-align: middle !important;
	                                            text-align: center;
                                         }
/** NAVIGATION GREY **/
#showbiz_news_1 .showbiz-navigation						{	margin-bottom:20px;}

#showbiz_news_1 .sb-navigation-left.notclickable:hover,
#showbiz_news_1 .sb-navigation-right.notclickable:hover,
#showbiz_news_1 .sb-navigation-left,
#showbiz_news_1 .sb-navigation-right,
#showbiz_news_1 .sb-navigation-play 	{	background: #919191;
                                    background: -moz-linear-gradient(top,  #919191 0%, #6b6b6b 100%); /* FF3.6+ */
                                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#919191), color-stop(100%,#6b6b6b));
                                    background: -webkit-linear-gradient(top,  #919191 0%,#6b6b6b 100%);
                                    background: -o-linear-gradient(top,  #919191 0%,#6b6b6b 100%);
                                    background: -ms-linear-gradient(top,  #919191 0%,#6b6b6b 100%);
                                    background: linear-gradient(to bottom,  #919191 0%,#6b6b6b 100%);
                                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#919191', endColorstr='#6b6b6b',GradientType=0 );
                                    border-top:1px solid #919191; border-left:1px solid #969696;  border-right:1px solid #969696; border-bottom:1px solid #6c6c6;

                                  }
#showbiz_news_1 .sb-navigation-left:hover,
#showbiz_news_1 .sb-navigation-right:hover,
#showbiz_news_1 .sb-navigation-play:hover		{	background: #919191;
                                            background: -moz-linear-gradient(top,  #6b6b6b 0%, #919191 100%); /* FF3.6+ */
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#6b6b6b), color-stop(100%,#919191));
                                            background: -webkit-linear-gradient(top, #6b6b6b 0% ,#919191 100%);
                                            background: -o-linear-gradient(top,  #6b6b6b 0% ,#919191 100%);
                                            background: -ms-linear-gradient(top,  #6b6b6b 0% ,#919191 100%);
                                            background: linear-gradient(to bottom,  #6b6b6b 0% ,#919191 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6b6b6b', endColorstr='#919191',GradientType=0 );
                                          } 
			</style>
						
			<div id="showbiz_news_1" class="showbiz-container" style="margin:0px auto;margin-top:0px;margin-bottom:0px;">
				
									<!-- start navigation -->
					<div class="showbiz-navigation center">
	<a id="showbiz_left_news" class="sb-navigation-left"><i class="sb-icon-left-open"></i></a>
	<a id="showbiz_play_news" class="sb-navigation-play"><i class="sb-icon-play sb-playbutton"></i><i class="sb-icon-pause sb-pausebutton"></i></a>					
	<a id="showbiz_right_news" class="sb-navigation-right"><i class="sb-icon-right-open"></i></a>
	<div class="sbclear"></div>
</div>					<!--  end navigation -->
								
				<div class="showbiz"  data-left="#showbiz_left_news" data-right="#showbiz_right_news" data-play="#showbiz_play_news" >
					<div class="overflowholder">
					
						<ul>					<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Bushmans-Grill.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Bushmans-Grill.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/styleswood-secures-2m-of-new-business-in-dubai/">Dutco Styles&Wood secures AED12.4mn of new business in Dubai</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">Property services firm DutcoStyles&amp;Wood has won two contracts worth over AED12.4mn in Dubai’s fast-growing hospitality sector.

The business’ joint venture with Dubai Transport Company, has secured the AED7.5mn refurbishment of the Jafza Convention Centre’s food court, and a AED5.5mn fit-out for luxury restaurant La Cantine.

Dutco Styles&amp;Wood will deliver a full fit-out at the 50,000 sq ft food court, situated on...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/styleswood-secures-2m-of-new-business-in-dubai/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Solar1.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Solar1.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/solar-panel-financing-deal/">Freetricity appoints Styles&Wood Energy as installer for solar panel financing deal</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">Styles&amp;Wood Group plc, the integrated property services and project delivery specialist, is pleased to announce that Freetricity, the leading UK solar supplier, has appointed Styles&amp;Wood Energy, the Group’s specialist renewable energy division, as the installer for its new financing deal which will fund free residential solar panels on up to 1500 houses a month....</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/solar-panel-financing-deal/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Lotus.png">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Lotus.png">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/tiddler-to-watch/">The Times Name Styles&Wood as ‘Tiddler to Watch’</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">Styles&amp;Wood jumped 7.7 per cent to 5/4p after being named among five contractors to refurbish post offices across the UK over a two-year period. No financial details were disclosed.  Shares in the shopfitter were cheaper than ever earlier in the month, after restructuring and redundancy costs dragged it into the red in the first...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/tiddler-to-watch/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Lotus.png">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Lotus.png">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/styleswood-wins-place-post-office/">Styles&Wood wins place on post office</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description"><b>REFURBISHMENT framework</b>
Styles &amp; Wood Group plc, the integrated property services and project delivery specialist, is pleased to announce that it has secured a place on the Post Office Crown Transformation Programme framework, which will span a two year period.
Styles&amp;Wood is one of five contractors appointed to the refurbishment framework but is the only firm...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/styleswood-wins-place-post-office/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/ir13b.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/ir13b.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/interim-statement/">Styles&Wood Group Plc - Interim Statement</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">Styles&amp;Wood Group Plc, the integrated property services and project delivery specialist announces its interim results for the six months ended 30th June 2013.

CEO tony Lenehan said "These results reflect a challenging period for Styles&amp;Wood. The Group has made significant investments as part of its diversification strategy to widen the service offering and enter into...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/interim-statement/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Member-Logo.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Member-Logo.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/uk-green-building-council/">UK Green Building Council</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">Styles&amp;Wood Join the UK Green Building Council
Integrated property services and project delivery specialist Styles&amp;Wood have become one of the newest members if the UK Green Building Council (UK-GBC).

Launched in 2007, registered charity UK-GBC was formed to radically improve the sustainability of the built environment, by transforming the way it’s planned, designed, constructed, maintained and...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/uk-green-building-council/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Chesterfield-WHU-Opening.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Chesterfield-WHU-Opening.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/hospital-refurbishment/">£1.2m Hospital Refurbishment</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">STYLES&amp;WOOD DELIVERS £1.2M HOSPITAL REFURBISHMENT – July 2013
Integrated property services and project delivery specialist Styles&amp;Wood has completed a £1.2 million clinical refurbishment scheme to create a new specialist women’s healthcare facility at Chesterfield Royal Hospital in Derbyshire.

The 21-week project involved the complete strip-out and remodelling of the hospital’s Women’s Health Unit and Trinity Ward,...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/hospital-refurbishment/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Lotus.png">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Lotus.png">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/800m-schools-framework/">£800m Schools Framework</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">STYLES&amp;WOOD LANDS PLACE ON £800M SCHOOLS FRAMEWORK   - JUNE 2013
Integrated property services and project delivery specialist Styles&amp;Wood was named as one of 10 contractors to have secured a place on an £800m national schools building framework.

The firm’s Projects division will now compete for work within public sector procurement consortium LHC’s Schools and Community Buildings...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/800m-schools-framework/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/10.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/10.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/dubai-hotel-scheme/">£2.6m Dubai Hotel Scheme</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">STYLES&amp;WOOD COMPLETES £2.6 MILLION DUBAI HOTEL SCHEME  - MAY 2013
Integrated property services and project delivery specialist Styles&amp;Wood has completed a £2.6 million (AED 14.8 million) refurbishment contract at Dubai’s newest hotel development, on behalf of JA Resorts and Hotels.

The 35,000 sq. ft. project included a comprehensive programme of works to convert a former residential...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/dubai-hotel-scheme/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/About4a.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/About4a.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/profits-up-at-styles-and-wood/">Profits Up At Styles&Wood</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">PROFITS UP AT STYLES&amp;WOOD APRIL 2013
Integrated property services and project delivery specialist Styles&amp;Wood has posted small rises in profit despite a dip in turnover.

The listed company, which provides services such as design and project delivery and also has a base at Lancaster University, posted operating profit of £2.2m in 2012, up year-on-year from £1.9m,...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/profits-up-at-styles-and-wood/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/i-FM-Award-Photo-300x200.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/i-FM-Award-Photo-300x200.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/ifm-technology-awards/">Top Honours iFM Technology Awards</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">&nbsp;
iSite take top honours at the iFM Technology Awards February 2013
The Assetology Hub, by iSite, part of Styles&amp;Wood Group PLC, has been awarded the much coveted ‘Commercial Services’ category at the annual i-FM Technology in FM Awards.

Revealed at this year’s Workspace Futures Awards at the Crystal in London earlier this week, the Awards recognise...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/ifm-technology-awards/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
									<li>
						<!-- THE MEDIA HOLDER -->
<div class="mediaholder">
  <div class="mediaholder_innerwrap">
    <img alt="" src="http://www.dutcosw.com/sw/wp-content/uploads/Liverpool_Exchange_Station.jpg">
      <!-- HOVER COVER CONAINER -->
      <div class="hovercover">
        <a class="fancybox" rel="group" href="http://www.dutcosw.com/sw/wp-content/uploads/Liverpool_Exchange_Station.jpg">
          <div class="linkicon"><i class="sb-icon-plus"></i></div>
        </a>
      </div><!-- END OF HOVER COVER -->
    </div>
  </div><!-- END OF MEDIA HOLDER CONTAINER -->

  <!-- DETAIL CONTAINER -->
  <div class="detailholder">
    <div class="divide20"></div>
    <div class="showbiz-title"><a href="http://www.dutcosw.com/exchange-station-revamp/">£5m Exchange Station Revamp</a></div>
    <div class="divide15"></div>
    <div class="showbiz-description">Styles&amp;Wood appointed to deliver £5m Exchange Station Revamp – January 2013
Integrated property services and project delivery specialist Styles&amp;Wood have commenced work on a £5m refurbishment of Liverpool’s Exchange Station.

The firm’s Projects division will deliver a scheme of work on behalf of developer Ashtenne Space Northwest, to give 190’000 sq ft office complex, known as...</div>
    <div class="divide15"></div>
    <a href="http://www.dutcosw.com/exchange-station-revamp/" class="showbiz-button">Read More</a>
  </div><!-- END OF DETAIL CONTAINER -->					</li>
					
				</ul>					
						<div class="sbclear"></div>
					</div> 
					<div class="sbclear"></div>
				</div>
				
								
			</div>
			
						
						
			<script type="text/javascript">
			
							
				jQuery(document).ready(function() {
					
					if(jQuery('#showbiz_news_1').showbizpro == undefined)
						showbiz_showDoubleJqueryError('#showbiz_news_1');

					jQuery('#showbiz_news_1').showbizpro({
						dragAndScroll:"on",
						carousel:"off",
						allEntryAtOnce:"off",
						closeOtherOverlays:"off",
						entrySizeOffset:0,
						heightOffsetBottom:0,
						conteainerOffsetRight:0,
						visibleElementsArray:[4,3,2,1],
						mediaMaxHeight:[0,0,0,0],
						rewindFromEnd:"on",
						autoPlay:"on",
						delay:"3000",
						speed:"300",
						easing:"Bounce.easeInOut"
					});

					jQuery(".fancybox").fancybox();
				});

			</script>
			
							
			<!-- END SHOWBIZ -->
			
			
</div>
</div>
</div></div></div>


















	<h2>Style &amp; Wood News</h2>
<script type="text/javascript" src="http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/js/sw-news.js"></script>
<link media="all" type="text/css" href="http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/css/sw-news.css" rel="stylesheet">

	<?php

$feedUrl = 'http://www.stylesandwood-group.co.uk/feed/';
$rawFeed = file_get_contents($feedUrl);
$xml = new SimpleXmlElement($rawFeed);
?>
	<div id="slider1">
		<a class="buttons prev" href="#">&#60;</a>
		<div class="viewport">
			<ul class="overview">
<?php 
	foreach ($xml->channel->item as $item){
		echo '<li><a href="'.$item->link.'">'.$item->title.'</a></li>';
	}
?>			</ul>
		</div>
		<a class="buttons next" href="#">&#62;</a>
	</div>
<?php
//////////////
?> 






















</div>



</section><!-- end #content -->				
	
	<footer id="footer">
		<div class="container">

			<div class="row"><div class="span4"><div id="text-3" class="widget widget_text"><h3 class="widgettitle title m_title">Dutco Styles&amp;Wood Legal</h3>			<div class="textwidget"><p><a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br />
<a href="http://www.dutcosw.com/contact/">Contact Us</a></p>
</div>
		</div></div><div class="span4"><div id="blog_subscription-3" class="widget jetpack_subscription_widget"><h3 class="widgettitle title m_title"><label for="subscribe-field"></label></h3>
		<form action="" method="post" accept-charset="utf-8" id="subscribe-blog-blog_subscription-3">
			<p id="subscribe-text"></p><p>Join 5 other subscribers</p>

			<p id="subscribe-email"><input type="text" name="email" value="Email Address" id="subscribe-field" onclick="if ( this.value == 'Email Address' ) { this.value = ''; }" onblur="if ( this.value == '' ) { this.value = 'Email Address'; }" /></p>

			<p id="subscribe-submit">
				<input type="hidden" name="action" value="subscribe" />
				<input type="hidden" name="source" value="http://www.dutcosw.com/news/" />
				<input type="hidden" name="sub-type" value="widget" />
				<input type="hidden" name="redirect_fragment" value="blog_subscription-3" />
								<input type="submit" value="Subscribe" name="jetpack_subscriptions_widget" />
			</p>
		</form>

		
</div></div><div class="span4"><div id="text-7" class="widget widget_text"><h3 class="widgettitle title m_title">Current Share Price </h3>			<div class="textwidget"><div class="SPG">
  <div class="spgBoxF">
    <div class="SPGInfo"> <span class="SWprice">
      101.00      </span> <span class="SPTxt">GBp </span><span class="SPTxt">
          </span> </div>
  </div>
  </a>
</div></div>
		</div></div></div><!-- end row -->	
			
			<div class="row">
				<div class="span12">
					<div class="bottom fixclear">
					
										
											
						<div class="copyright">
							
							
							<p>© 2013 Copyright - Dutco Styles&Wood</p>							
							
						</div><!-- end copyright -->
							
											

					</div><!-- end bottom -->
				</div>
			</div><!-- end row -->
			
		</div>
	</footer>
	    </div><!-- end page_wrapper -->
	
    <a href="#" id="totop">TOP</a> 
	<div style="display:none">
	</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44622877-1', 'dutcosw.com');
  ga('send', 'pageview');

</script><script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/core/js/hoverIntent.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uberMenuSettings = {"speed":"300","trigger":"hoverIntent","orientation":"horizontal","transition":"slide","hoverInterval":"20","hoverTimeout":"400","removeConflicts":"on","autoAlign":"off","noconflict":"off","fullWidthSubs":"on","androidClick":"on","iOScloseButton":"on","loadGoogleMaps":"off","repositionOnLoad":"on"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/ubermenu/core/js/ubermenu.min.js'></script>
<script type='text/javascript' src='http://s0.wp.com/wp-content/js/devicepx-jetpack.js'></script>
<script type='text/javascript' src='http://s.gravatar.com/js/gprofiles.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/jetpack/modules/wpgroho.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/js/bootstrap.min.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/js/plugins.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/addons/superfish_responsive/superfish_menu.js'></script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/addons/prettyphoto/jquery.prettyPhoto.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var zn_do_login = {"ajaxurl":"http:\/\/www.dutcosw.com\/sw\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/themes/DutcoSW/js/znscript.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uberMenuStickySettings = {"expand_menu_bar":"1","offset":"0","mobile":"","scroll_context":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.dutcosw.com/sw/wp-content/plugins/ubermenu-sticky/ubermenu.sticky.js'></script>
<script type="text/javascript">
						(function($){ 
							$(window).load(function(){
								/* Activate Superfish Menu */
								var sfDelay = 600;
								if($('html').hasClass('isie'))
									sfDelay = 300;
								$('#main_menu > ul')
								.supersubs({ 
									minWidth:    12,   // minimum width of sub-menus in em units 
									maxWidth:    27,   // maximum width of sub-menus in em units 
									extraWidth:  1     // extra width can ensure lines don't sometimes turn over 
								}).superfish({
									delay:sfDelay,
									dropShadows:false,
									autoArrows:true,
									speed:300
								}).mobileMenu({
									switchWidth: 960,
									topOptionText: 'SELECT A PAGE',
									indentString: '&nbsp;&nbsp;&nbsp;'
								});
							});
						})(jQuery);
						</script>
	<script src="http://stats.wordpress.com/e-201428.js" type="text/javascript"></script>
	<script type="text/javascript">
	st_go({v:'ext',j:'1:2.9.3',blog:'53739644',post:'124',tz:'0'});
	var load_cmc = function(){linktracker_init(53739644,124,2);};
	if ( typeof addLoadEvent != 'undefined' ) addLoadEvent(load_cmc);
	else load_cmc();
	</script></body>
</html>