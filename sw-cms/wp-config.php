<?php


/** Enable W3 Total Cache Edge Mode */
define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
ini_set('memory_limit', '256M');
define('DB_NAME', 'swdutco_wp2015');

define('DB_USER', 'wordpress_admin');

/** MySQL database password */
define('DB_PASSWORD', 'CXv7AFsCzKhbFTY6');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8vlS98ApgHwYpyYHRZ0opYLzxAQkG9E8le1uoHJEhfd3UMOANEkyZet6w3Xz72j0');
define('SECURE_AUTH_KEY',  '62Fi7yVBM9LcYnYk5VSNYkYS6EUtxLIBVolQ_NL2xSlX4v1moUwn8e6EfQ49d7L2');
define('LOGGED_IN_KEY',    'njxJmWml8TqHCOm506PRWoxS4MBKgK1jaVml1mss6C8YPEcibBJkC7deXX0K0uot');
define('NONCE_KEY',        'rFabkKH0aV1smVaV4Vn6F6FZQz737pa51MaBdfJlaifjWz3He9AKE_veECS5AjHT');
define('AUTH_SALT',        'bAf_0ABkGyCHw0eRwMoP6slz8nMzH9x10jKP3pRzDl46KIZIQPJY6JAJWhQs5oBG');
define('SECURE_AUTH_SALT', 'izneSqR7upgzMCsHqZJ6CHYRcpu5wy0VtEZ0nSDgbSrf91YmMN_DnD7N6uh3w5uj');
define('LOGGED_IN_SALT',   'eo5Hp4VsvvzpQm2DXuXpFaodY0o4XOEsfSC9qgs3HHnWelMNKDJWVkXIXQczytnJ');
define('NONCE_SALT',       'cjut8LSeY2tZe7slvgE3dIl3fdakj4qz7acH01UTnJdix6EYYNJNRfn6zBiHYvyc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cms_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');