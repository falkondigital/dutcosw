<?php
/**
 * @package Eword
 */
/*
Plugin Name: Eword setup
Description: Eword post type setup
Version: 1.0
Author: T.Giblin / A. Mursec
*/


function eword_init() {
    /** Add Portfolio Post Type **/
    $labels = array(
        'name' => 'Portfolios',
        'singular_name' => 'Portfolio',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Portfolio',
        'edit_item' => 'Edit Portfolio',
        'new_item' => 'New Portfolio',
        'all_items' => 'All Portfolios',
        'view_item' => 'View Portfolio',
        'search_items' => 'Search Portfolios',
        'not_found' =>  'No portfolios found',
        'not_found_in_trash' => 'No portfolio found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Portfolios'
    );
    
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array( 'slug' => 'portfolios', 'with_front' => false),
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'page-attributes','title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields')
    ); 
    
    register_post_type( 'portfolio', $args );
    
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => 'Categories',
		'singular_name'     => 'Category',
		'search_items'      => 'Search Categories',
		'all_items'         => 'All Categories',
		'parent_item'       => 'Parent Category',
		'parent_item_colon' => 'Parent Category:',
		'edit_item'         => 'Edit Category',
		'update_item'       => 'Update Category',
		'add_new_item'      => 'Add New Category',
		'new_item_name'     => 'New Category Name',
		'menu_name'         => 'Categories',
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'portfolios' ),
	);

	register_taxonomy( 'portfolio_entries', array( 'portfolio' ), $args );    
    
    /** Add Portal Post Type **/
    $labels = array(
        'name' => 'Portal',
        'singular_name' => 'Portal',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Portal',
        'edit_item' => 'Edit Portal',
        'new_item' => 'New Portal',
        'all_items' => 'All Portals',
        'view_item' => 'View Portal',
        'search_items' => 'Search Portals',
        'not_found' =>  'No portals found',
        'not_found_in_trash' => 'No portal found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Portal'
    );
    
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array( 'slug' => 'portal', 'with_front' => false),
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array( 'page-attributes','title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields')
    );
    
    register_post_type( 'portal', $args );
    
    
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => 'Tools',
		'singular_name'     => 'Tool',
		'search_items'      => 'Search Tools',
		'all_items'         => 'All Tools',
		'parent_item'       => 'Parent Tool',
		'parent_item_colon' => 'Parent Tool:',
		'edit_item'         => 'Edit Tool',
		'update_item'       => 'Update Tool',
		'add_new_item'      => 'Add New Tool',
		'new_item_name'     => 'New Tool Name',
		'menu_name'         => 'Tools',
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'tools' ),
	);

	register_taxonomy( 'portal_cats', array( 'portal' ), $args );  
    
}
add_action( 'init', 'eword_init' );


function flush_rules() {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
}

function callMetas() {
    
    Metas::$boxes = array(
        array(
            'id' => 'slider',
            'title' => 'Slider',
            'context' => 'side',
            'priority' => 'core',
            'args' => array(
                'type' => 'input',
                'id' => 'slider',
                'description' => 'Input the ID of the slider you wish to appear'
            ),
            'restrict' => 'all',
            'type' => array('page')
        )


    );
    new Metas();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'callMetas' );
    add_action( 'load-post-new.php', 'callMetas' );
}

class Metas {
    public static $boxes;
    /**
    * hook actions on initiate
    */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ));
	add_action( 'save_post', array( $this, 'save' ) );
    }
    /**
    * Adds the meta box container.
    */
    public function add_meta_box($post_type ) {
        global $post;

            //loops boxes and adds them to the post type
            foreach(self::$boxes as $meta_box) {
                
                //limit meta box to certain post types
                if ( in_array( $post_type, $meta_box['type'])) {                
                    //restriction for pages or all pages
                    if (isset($meta_box['restrict']) && ($meta_box['restrict'] == 'all' || $meta_box['restrict'] == $post->ID)) {
                        add_meta_box(
                            $meta_box['id'],
                            $meta_box['title'],
                            array( $this, 'render_meta_box_content' ),
                            $post_type,
                            $meta_box['context'],
                            $meta_box['priority'],
                            $meta_box['args']
                        );
                    }
                }
            }
    }

    /**
    * Save the meta when the post is saved.
    *
    * @param int $post_id The ID of the post being saved.
    */
    public function save( $post_id ) {

        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */
        foreach(self::$boxes as $meta_box) {
            //limit meta box to certain post types
            if (isset($_POST['post_type']) &&  in_array( $_POST['post_type'], $meta_box['type'])) {
                //restriction for pages or all pages
                if (isset($meta_box['restrict']) && ($meta_box['restrict'] == 'all' || $meta_box['restrict'] == $_POST['ID'])) {
                    // Check if our nonce is set.
                    if ( ! isset( $_POST[$meta_box['args']['id'].'_custom_box_nonce'] ) )
                            return $post_id;
            
                    $nonce = $_POST[$meta_box['args']['id'].'_custom_box_nonce'];
            
                    // Verify that the nonce is valid.
                    if ( ! wp_verify_nonce( $nonce, $meta_box['args']['id'].'_custom_box' ) )
                            return $post_id;
            
        
                    // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
                    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                            return $post_id;
            
                    // Check the user's permissions.
                    if ( 'page' == $_POST['post_type'] ) {
            
                            if ( ! current_user_can( 'edit_page', $post_id ) )
                                    return $post_id;
            
                    } else {
            
                            if ( ! current_user_can( 'edit_post', $post_id ) )
                                    return $post_id;
                    }
            
                    /* OK, its safe for us to save the data now. */
            
                    // Sanitize the user input.
                    
                    if ($meta_box['args']['type'] == 'table') {
                        $_POST[$meta_box['args']['id'].'_field'] = implode("|", $_POST[$meta_box['args']['id'].'_field']);
                    }
                    
                    if ($meta_box['args']['type'] == 'textarea') {
                        $mydata = esc_html($_POST[$meta_box['args']['id'].'_field'] );
                    } else {
                        $mydata = sanitize_text_field( $_POST[$meta_box['args']['id'].'_field'] );
                    }
            
                    // Update the meta field.
                    update_post_meta( $post_id, '_'.$meta_box['args']['id'], $mydata );
                }
            }
        }
       
    }


    /**
    * Render Meta Box content.
    *
    * @param WP_Post $post The post object.
    */
    public function render_meta_box_content( $post,$args ) {
        global $wpdb;
        $args = $args['args'];

        // Add an nonce field so we can check for it later.
        wp_nonce_field( $args['id'].'_custom_box', $args['id'].'_custom_box_nonce' );

        // Use get_post_meta to retrieve an existing value from the database.
        $value = get_post_meta( $post->ID, '_'.$args['id'], true );

        // Display the form, using the current value.
        echo '<label for="events_'.$args['id'].'_field">';
        _e( $args['description'], '' );
        echo '</label> ';
        switch ($args['type']) {
            case "input":
                echo '<input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field"';
                echo ' value="' . esc_attr( $value ) . '" />';
            break;
            case "textarea":
                echo '<textarea style="width:100%; height:125px;" id="'.$args['id'].'_field" name="'.$args['id'].'_field" >' . esc_html( $value ) . '</textarea>';
            break;
            case "table":
                $value = explode('|',$value);
                echo '<table cellspacing="0" cellpadding="5" width="100%" style="border:1px solid #eee;">';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Monday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[0] ) . '" /></th>
                </tr>';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Tuesday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[1] ) . '" /></th>
                </tr>';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Wednesday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[2] ) . '" /></th>
                </tr>';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Thursday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[3] ) . '" /></th>
                </tr>';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Friday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[4] ) . '" /></th>
                </tr>';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Saturday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[5] ) . '" /></th>
                </tr>';
                echo '<tr>
                        <th style="border:1px solid #eee; text-align:left;">Sunday</th>
                        <td style="border:1px solid #eee;text-align:left;"><input style="width:100%;" type="text" id="'.$args['id'].'_field" name="'.$args['id'].'_field[]" value="' . esc_attr( $value[6] ) . '" /></th>
                </tr>';
                
                echo '</table>';
            break;
            case "select":
                echo '<select style="width:100%;" id="'.$args['id'].'_field" name="'.$args['id'].'_field" >';
                    echo '<option '.((esc_attr( $value ) == 1) ? 'selected="selected"' : '').' value="1">Yes</option>';
                    echo '<option '.((esc_attr( $value ) == 0) ? 'selected="selected"' : '').' value="0">No</option>';
                echo '</select>';
            break;
            case "select-custom":
                echo '<select style="width:100%;" id="'.$args['id'].'_field" name="'.$args['id'].'_field" >';
                    foreach($args['values'] as $color) {
                        echo '<option '.((esc_attr( $value ) == $color) ? 'selected="selected"' : '').' value="'.$color.'">'.ucwords($color).'</option>';
                    }
                echo '</select>';
		   
            break;
        }

    }
    
}


