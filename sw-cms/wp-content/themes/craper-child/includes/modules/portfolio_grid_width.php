<?php global $wp_query;

$t = $GLOBALS['_sh_base'];
$paged = get_query_var('paged');

$data_filtration = '';
$data_posts = '';
?>

<?php //$query = new WP_Query( array('post_type'=>'sh_portfolio', 'post_status'=>'publish') ); 
	if( have_posts() ):
	
		ob_start();?>
		
	<ul class="filter-list">
	
		<?php $count = 0; 
		$fliteration = array();?>
		<?php while( have_posts() ): the_post(); 
			$meta = get_post_meta( get_the_id(), '_sh_portfolio_meta', true );//printr($meta);
			$post_terms = get_the_terms( get_the_id(), 'portfolio_category'); //printr($post_terms);
			foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
			$temp_category = get_the_term_list(get_the_id(), 'portfolio_category', '', ', ');
		?>
		
		<?php
			$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()));
			//$url = $thumb['0'];
		?>

		<?php $post_terms = wp_get_post_terms( get_the_id(), 'portfolio_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.' ';?>
		
		<li class="mix <?php echo $term_slug; ?>">
			<div class="portfolio-box">
				<figure> <a href="<?php echo $url;?>" data-rel="prettyPhoto" title="showcase image"> <?php the_post_thumbnail('306x250');?></a>
					<figcaption>
						<h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
						<p><?php echo get_the_term_list(get_the_id(), 'portfolio_category', '', ', '); ?></p>
					</figcaption>
				</figure>
			</div>
			<div class="portfolio-detail">
				<h4>Project Description</h4>
				<p><?php the_excerpt(); ?></p>
				<div class="portfolio-btm">
					<h4>Client</h4>
					<ul>
						<li><?php echo sh_set($meta, 'client_name');?> </li>
						<li>website <a href="<?php echo sh_set($meta, 'project_url');?>"><?php echo sh_set($meta, 'project_url');?></a></li>
					</ul>
					<a href="<?php the_permalink();?>" class="btn btn-small btn-light-dark btn-icon-hold"><i class="icon-page-arrow-right "></i><span>View Details</span></a> </div>
			</div>
		</li>
		
		
		
		<?php endwhile; ?>
		
	</ul>
	
<?php $data_posts = ob_get_contents();
ob_end_clean();

endif; 
ob_start();?>

<article role="main">
	<div class="section">
	<div class="container"> 
		
		<!-- portfolio-area -->
		<div class="portfolio-area">
			<header class="heading">
				<?php $terms = get_terms(array('portfolio_category')); ?>
            	<?php if( $terms ): ?>
				<ul class="filter-tabs">
					<li class="layout-list" data-role="button"><i class="icon-list"></i></li>
					<li class="layout-grid active" data-role="button"><i class="icon-grid"></i></li>
					<li class="filter" data-role="button" data-filter="all">All</li>
					<?php foreach( $fliteration as $t ): ?>
					<li class="filter" data-role="button" data-filter="<?php echo sh_set( $t, 'slug' ); ?>"><?php echo sh_set( $t, 'name'); ?></li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>
			</header>
			<div class="portfolio-list">
				
				<?php echo $data_posts; ?>
				
			</div>
			<div class="container">
				
				<?php _the_pagination(); ?>
				
			</div>
		</div>
		<!-- /portfolio-area --> 
		
	</div>
	</div>
</article>

<?php $output = ob_get_contents();
ob_end_clean();
