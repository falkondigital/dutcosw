<?php if($view=='with_sidebar'){?>
<div class="related-projects col-2">
	
	<h3><?php _e('Related posts', SH_NAME); ?></h3>  
	
	<?php  
		
		$tags = wp_get_post_terms(get_the_id(), 'portfolio_category');  
		
		$tag_ids = array();  
		
		if ($tags) {  
			
			foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;  
		}
		
		$args = 
		array (
			//'tax_query' => array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $tag_ids)),  
			'post__not_in' => array(get_the_id()),  
			'posts_per_page'=>4, // Number of related posts to display.  
			'ignore_sticky_posts'=>1  
		);
		if( $tag_ids ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $tag_ids));

		$related_posts = new WP_Query( $args );
		//printr($related_posts);
		echo '<ul>';
		foreach($related_posts->posts as $rel ):
		
		echo '<li>
				<div class="portfolio-box">
					<figure> <a href="'.get_template_directory_uri().'/images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"> '.get_the_post_thumbnail($rel->ID, '306x250').'</a>
						<figcaption>
							<h5><a href="'.get_permalink( $rel->ID ).'">'.get_the_title($rel->ID).'</a></h5>
							<p>'.get_the_term_list(get_the_id(), 'portfolio_category', '', ', ').'</p>
						</figcaption>
					</figure>
				</div>
			</li>';
			
		endforeach;
	echo '</ul>';
	///$post = $orig_post;  
	wp_reset_query(); 
	?>  
</div>
<?php }elseif($view=='with_comments'){?>
<div class="related-projects col-4">
    				
	<h3>Related posts</h3>  
	
	<?php  
		
		$tags = wp_get_post_terms(get_the_id(), 'portfolio_category');  
		
		$tag_ids = array();  
		
		if ($tags) {  
			
			foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;  
		}
		
		$args = 
		array (
			//'tax_query' => array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $tag_ids)),  
			'post__not_in' => array(get_the_id()),  
			'posts_per_page'=>4, // Number of related posts to display.  
			'caller_get_posts'=>1  
		);
		if( $tag_ids ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $tag_ids));

		$related_posts = new WP_Query( $args );
		//printr($related_posts);
		echo '<ul>';
		foreach($related_posts->posts as $rel ):
		
		echo '<li>
				<div class="portfolio-box">
					<figure> <a href="'.get_template_directory_uri().'/images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"> '.get_the_post_thumbnail($rel->ID, '306x250').'</a>
						<figcaption>
							<h5><a href="'.get_permalink( $rel->ID ).'">'.get_the_title($rel->ID).'</a></h5>
							<p>'.get_the_term_list(get_the_id(), 'portfolio_category', '', ', ').'</p>
						</figcaption>
					</figure>
				</div>
			</li>';
			
		endforeach;
	echo '</ul>';
	///$post = $orig_post;  
	wp_reset_query(); 
	?>  
</div>
<?php }elseif($view=='simple'){?>
<div class="related-projects">
		
		<h3>Related posts</h3>  
		
		<?php  
			
			$tags = wp_get_post_terms(get_the_id(), 'portfolio_category');  
			
			$tag_ids = array();  
			
			if ($tags) {  
				
				foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;  
			}
			
			$args = 
			array (
				//'tax_query' => array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $tag_ids)),  
				'post__not_in' => array(get_the_id()),  
				'posts_per_page'=>4, // Number of related posts to display.  
				'caller_get_posts'=>1  
			);
			if( $tag_ids ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $tag_ids));

			$related_posts = new WP_Query( $args );
			//printr($related_posts);
			echo '<div class="portfolio-list"><ul>';
			foreach($related_posts->posts as $rel ):
			
			echo '<li>
					<div class="portfolio-box">
						<figure> <a href="'.get_template_directory_uri().'/images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"> '.get_the_post_thumbnail($rel->ID, '306x250').'</a>
							<figcaption>
								<h5><a href="'.get_permalink( $rel->ID ).'">'.get_the_title($rel->ID).'</a></h5>
								<p>'.get_the_term_list(get_the_id(), 'portfolio_category', '', ', ').'</p>
							</figcaption>
						</figure>
					</div>
				</li>';
				
			endforeach;
		echo '</ul></div>';
		///$post = $orig_post;  
		wp_reset_query(); 
		?>  
	</div>
<?php }?>