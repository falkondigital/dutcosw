<?php get_header(); 
$theme_options = _WSH()->option(); 
$sidebar = sh_set( $theme_options, '404_sidebar' );?>

<article class="banner">
	<div class="container">
		<h2><?php wp_title(''); ?></h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
				<?php echo get_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</article>

<article role="main">
	<div class="section">
		<div class="container">
        
        	<div class="row">
			
                <div class="error-box<?php echo ( $sidebar ) ? ' col-xs-12 col-sm-8 col-md-8' : ' col-xs-12 col-sm-8 col-md-12'; ?>"> 
                    <?php if( $nf_image = sh_set( $theme_options, '404_image' ) ): ?>
                        <img src="<?php echo $nf_image; ?>" alt="error">
                    <?php else: ?>
                        <img src="<?php echo get_template_directory_uri();?>/images/error.png" alt="error">
                    <?php endif; ?>
                    
                    <p>
                        <?php if( $nf_title = sh_set( $theme_options, '404_title' ) ): ?>
                            <h2><?php echo $nf_title; ?></h2>
                        <?php endif; ?>
                        
                        <?php if( $nf_title = sh_set( $theme_options, '404_content' ) ): ?>
                            <p><?php echo $nf_title; ?></p>
                        <?php else: ?>
                            <?php _e('<strong>Uuuuuuups</strong>, we are sorry but this page does not exist.', SH_NAME); ?>
                        <?php endif; ?>
                    </p>
                    
                    <?php if( $nf_title = sh_set( $theme_options, '404_home_button' ) ): ?>
                        <a href="<?php echo home_url();?>" class="btn btn-primary btn-icon-hold"><i class="icon-page-arrow-right "></i><span><?php _e('Go Back to Home page', SH_NAME);?></span></a> 
                    <?php endif; ?>
                    
                </div>
                
                <?php if( $sidebar ): ?>
            		<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    	<?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
		</div>
	</div>
</article>

<?php echo do_shortcode('[sh_tweet_check_out]');?>	

<?php get_footer(); ?>