<?php get_header();?>
<?php $theme_options = _WSH()->option(); 
$sidebar = sh_set( $theme_options, 'category_sidebar');
$layout = sh_set($theme_options, 'category_sidebar_layout', 'full');
$view = sh_set($theme_options, 'blog_page_view_list', 'list');
$view = sh_set( $_GET, 'blog_view' ) ? sh_set( $_GET, 'blog_view' ) : $view;
?>
<article class="banner">
	<div class="container">
        <h2><?php
		
				/*
				 * Queue the first post, that way we know what author
				 * we're dealing with (if that is the case).
				 *
				 * We reset this later so we can run the loop properly
				 * with a call to rewind_posts().
				 */
				
				single_cat_title(__( 'Category Archive : ', SH_NAME ));?></h2>
        
		<div class="breadcrumbs">
			
            <ul class="breadcrumb">
            
				<?php //echo get_the_breadcrumb(); ?>
			
            </ul>
		
        </div>
        
	</div>
</article>

<article role="main">
	<div class="section">
		<div class="container">
			<div class="row">
				
				<?php 
				
$layout = 'right';
$sidebar = 'blog';
$view = 'grid';
				
				$class = ( $layout == 'full' ) ? 'blog-centered' : 'col-xs-12 col-sm-8 col-md-8';
				  $class = ( $layout == 'full'& $view == 'grid' ) ? 'col-xs-12 col-sm-8 col-md-12' : 'col-xs-12 col-sm-8 col-md-8';
				  $column = ( $layout == 'full' ) ? 'col-3' : 'col-2';
				  ?>
				<?php if( $sidebar && $layout == 'left' ): ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
            	<?php endif; ?>

				<div class="<?php echo $class; ?>">
					<div class="contents">
							
							<?php if( $view == 'grid' ) get_template_part('content', 'masonry'); 
							
							elseif( $view == 'list' ) get_template_part('content', 'blog'); ?>

							<?php _the_pagination(); ?>
						
					</div>
				</div>
				
				<?php if( $sidebar && $layout == 'right' ): ?>
					<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    	<?php dynamic_sidebar( $sidebar ); ?>
                	</div>
            	<?php endif; ?>

			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
