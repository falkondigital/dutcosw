<footer>
	<div class="container">
		<div class="row">

			<!--			<div class="col-xs-12 col-md-4">-->
			<!--				<h4 class="title">Styles&amp;Wood Group Links</h4>-->
			<!--				<p><a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br />-->
			<!--					<a href="http://www.stylesandwood-group.co.uk/sitemap/">Sitemap</a><br />-->
			<!--					<a href="http://www.stylesandwood-group.co.uk/contact/">Contact Us</a></p>-->
			<!--			</div>-->


            <div class="col-md-4">
                <h2 style="color:#fff;">Useful Links</h2>

                <p>
                    <a href="http://www.stylesandwood-group.co.uk/" target="_blank" rel="nofollow">Styles&amp;Wood Group plc</a><br>
                    <a href="http://www.stylesandwood.co.uk/" target="_blank" rel="nofollow">Styles&amp;Wood</a><br>
                    <a href="http://www.keysource.co.uk/ " target="_blank" rel="nofollow">Keysource</a><br>
                    <a href="http://www.gdmpartnership.co.uk/ " target="_blank" rel="nofollow">GDM Partnership</a><br>
                </p>


            </div>


            <div class="col-md-4">

                <br>
                <p style="padding-top:16px;">
                <a href="<?php echo home_url(); ?>/contact/">Contact Us</a><br>
                <a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br>
                <a href="http://www.stylesandwood.co.uk/joinus/" target="_blank">Vacancies</a><br>
                    <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Modern-Slavery-Statement.pdf" target="_blank">Modern Slavery Statement </a><br>
                    <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Health-Safety.pdf" target="_blank">Health & Safety Policy </a><br>
                    <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Quality-Policy.pdf" target="_blank">Quality Policy </a><br>
                    <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Environmental-Policy.pdf" target="_blank">Environmental Policy</a>
                </p>
             </div>

            <div class="col-md-4">
<!--                <h2 style="color:#fff;">Current Share Price</h2>-->
<!--				<iframe class="share-iframe" frameborder=0 src="http://ir.tools.investis.com/Clients/uk/styles_and_wood1/Ticker/Ticker.aspx?culture=en-GB"></iframe><br>-->
			</div>


			<?php dynamic_sidebar('footer-sidebar'); ?>
		</div>

		<div class="row">
			<div class="span12">
				<div class="bottom fixclear">

					<ul class="social-icons colored fixclear"><li class="title">See more from the Styles&amp;Wood Group</li><li class="social-twitter"><a target="_blank" href="https://twitter.com/StylesandWood">Twitter</a></li><li class="social-linkedin"><a target="_blank" href="http://www.linkedin.com/company/78033">LinkedIn</a></li></ul>

					<div class="copyright">


						<a href="http://www.stylesandwood-group.co.uk"><img alt="Styles&amp;Wood Group PLC" src="http://stylesandwood.co.uk/sw-cms/wp-content/uploads/SylesWood-sm.png"></a><p>&copy; 2017 Copyright - Styles&amp;Wood Group PLC</p>

					</div><!-- end copyright -->



				</div><!-- end bottom -->
			</div>
		</div>

	</div>
</footer>
</div>
<!-- /pageWrapper -->

<?php wp_footer(); ?>

<!-- Don't forget analytics -->

</body>

</html>
