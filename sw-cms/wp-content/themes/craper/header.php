<?php $theme_options = _WSH()->option();?>
<!DOCTYPE html>
<!--[if lt IE 10]> <html class="ie-lt-10"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<html <?php language_attributes(); ?>>

<head>
	
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if( is_home() || is_front_page() ) {
				  echo get_bloginfo('name').' - '.get_bloginfo('description');
			  }else wp_title( '' ); echo ' - '.get_bloginfo('name');
		   ?>
	</title>
		<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' id='font-awesome-css'  href='/sw-cms/wp-content/themes/craper/css/fonts/font-awesome.css' type='text/css' media='all' />
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
    <?php if( $favicon = sh_set($theme_options, 'favicon') ): ?>
		<link rel="shortcut icon" href="<?php echo $favicon;?>" type="image/x-icon" />
	<?php endif; ?>
	
		<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50525861-3', 'auto');
  ga('send', 'pageview');

</script>


	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	
	<div class="pageWrapper">
	<div class="top-strip">
        <div class="container">
            <div class="pull-left">
                <ul class="inline">
                
                	<?php if( $call_us = sh_set($theme_options, 'header_phone') ): ?>
                    	<li><i class="icon-phone"></i><strong><?php _e('Call us', SH_NAME); ?></strong> <?php echo $call_us;?></li>
                    <?php endif; ?>
                    
                    <?php if( $send_mail = sh_set($theme_options, 'header_email') ): ?>
                    	<li><i class="icon-mail"></i><strong><?php _e('Send email', SH_NAME); ?></strong> <a href="mailto:<?php echo sanitize_email($send_mail);?>"><?php echo sanitize_email($send_mail);?></a>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="pull-right">
                <ul class="social-links">
                    <?php echo sh_get_social_icons(); ?>
                </ul>
            </div>
        </div>
    </div>
	<div class="logo-bar">
        <div class="container"> 
            
            <!-- Logo -->
            <div class="logo"> 
                
                <a href="<?php echo home_url(); ?>">
                    <?php if( sh_set($theme_options, 'logo') ): ?>
                        <img alt="Logo" src="<?php echo sh_set($theme_options, 'logo' );?>" />
                    <?php else: ?>
                        <img alt="Logo" src="<?php echo get_template_directory_uri().'/images/logo.png';?>" />
                    <?php endif; ?>
                </a> 
                
            </div>
            <div class="pull-right"> <span class="nav-button"></span>
                <nav class="main-nav">
                    <?php
						wp_nav_menu(
							array(
								'container' => false, /* classes for container */
								'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',	
								'theme_location' => 'main_menu', /* we have two menus here we use our header menu as primery navigation */
								'depth' => 0, /* in depth of 3 we'r using dropdown menu */
								'walker' => new SH_Bootstrap_walker()
								
							)
						);?>
                </nav>
            </div>
        </div>
    </div>