<?php

class SH_Shortcodes
{
	protected $keys;
	protected $toggle_count = 0;
	protected $child;
	protected $path;
	
	function __construct()
	{
		$GLOBALS['sh_toggle_count'] = 0;
		add_action('init', array( $this, 'add' ) );
		$this->child = get_stylesheet_directory();
		$this->path = get_template_directory();
	}
	
	function add()
	{
		//include(get_template_directory().'/includes/resource/shortcodes.php');
		$options = array( 'fun_facts','fact','how_we_work','portfolio','block_title','testimonial','clients','ready_to_start',
						  'some_features','feel_the_power','features_list','let_start','endless_space','endless_space_ra','our_sponsors','latest_from_3_categories','latest_from_blog',
						  'multi_purpose','fact_buy_theme','build_your_website','product_video','awesome_features_list',
						  'awesome_feature','goodlooking_mockup','user_quicktips','latest_projects','what_we_do','fun_facts_both',
						  'fact_both','team','work_with_us','single_portfolio','blue_ribbon','ptable_container','ptable','ptable_features',
						  'portfolio_full_width','tweet_check_out','portfolio_grid_width','portfolio_grid_three','portfolio_grid_four',
						  'portfolio_with_sidebar','office','blockquote','fun_facts_both_parallex','fact_both_parallex',
						  'we_do_for_you','working_process','button','button_with_icon','alert','blockquote_half','fact_buy_theme_half',
						  'Get_in_touch','normal_text_box','office_address'
						  
					
		);

		$this->keys = $options;//array_keys( $options );
		
		foreach( $this->keys as $k )
		{
			if( method_exists( $this, $k ) ) add_shortcode( 'sh_'.$k, array( $this, $k ) );
		}
	}
	
	/**
	 * This shortcodes is used on homepage 1 in html version .. it shows services with font awesome icons
	 * with a little descriptions in grid view.
	 */
	function fun_facts($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'num' => 4,
		
		), $atts ) );
		
		return '<div class="container"> <div class="info-blocks margin-top-minus80">'. do_shortcode( $contents ) .'</div></div>';
		return $output;
	}
	
	function fact($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'num' => '',
			'link' => '',
			'icon' => '',
			'icon_size' => '',
		
		), $atts ) );
		$startlink = '<a href="'.$link.'">';
		$endlink = '</a>';
		if($num){
			
			return
			'<div class="info-block animated out" data-animation="flipInX" data-delay="0"> 
				<strong>'.$num.'</strong>
				<p>'.$title.'</p>
			</div>';

		}else{
			if (!$link) {
				$startlink = '';
				$endlink = '';
			}
			return
			'<div class="info-block animated out" data-animation="bounceIn" data-delay="0">
             	'.$startlink.'<div class="iconic"><i class="icon-'.$icon.' icon-'.$icon_size.'x theme-color"></i></div>
                <p>'.$title.'</p>'.$endlink.'
         	</div>';

		} 
		
		
			
	}
	
	function how_we_work($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('How we Work', SH_NAME),
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		
		query_posts($args);
		
		$services = 1;
		
		include( _WSH()->includes( 'includes/modules/how_we_work.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function portfolio($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Latest Projects', SH_NAME),
			'num' => 10,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
			'button_text' => 'View All Projects',
			'button_link' => '',

		), $atts ) );
		
		wp_enqueue_script(array('jquery-prettyPhoto'));
		
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		
		query_posts($args);
		
		$services = 1;

		include( _WSH()->includes( 'includes/modules/portfolio.php' ) );
		wp_reset_query() ;
		return $output;
	}
	
	function block_title($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
		), $atts ) );
		
		return 
		'<div class="section no-padding-bottom"> 
            <div class="clients-area">
                <div class="container">
					<header class="heading">
        				<h2 class="animated out" data-animation="fadeInUp" data-delay="0">'.$title.'</h2>
        			</header>
		';
	}
	
	function testimonial($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
			'pagination' => 'top',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-carouFredSel-6-2-1-packed'));
		
		$args = array('post_type' => 'sh_testimonial', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'testimonial_category','field' => 'id','terms' => $cat));
		
		query_posts($args);
		
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/testimonial.php');
		include( _WSH()->includes( 'includes/modules/testimonial.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function clients($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'num' => 8,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-carouFredSel-6-2-1-packed'));
		
		$args = array('post_type' => 'sh_clients', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/clients.php');
		include( _WSH()->includes( 'includes/modules/clients.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function ready_to_start($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			'left_btn_text' => '',
			'left_btn_link' => '',
			'right_btn_text' => '',
			'right_btn_link' => '',
			'img' => '',
			
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/ready_to_start.php');
		include( _WSH()->includes( 'includes/modules/ready_to_start.php' ) );
		
		wp_reset_query() ;
		return $output;

	}
	
	function some_features($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' =>  __('Some Features', SH_NAME),
			), $atts ) );
		
		return
		'<div class="section no-padding-bottom">
            <div class="container">
                <header class="heading">
                    <h2>Some Features</h2>
                </header>';
	}
	
	function feel_the_power($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			'img' => '',
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/feel_the_power.php');
		include( _WSH()->includes( 'includes/modules/feel_the_power.php' ) );
		
		wp_reset_query() ;
		return $output;

	}
	
	function features_list($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/features_list.php');
		include( _WSH()->includes( 'includes/modules/features_list.php' ) );
		
		wp_reset_query() ;
		return $output;
		
		
	}
	
	function let_start($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Are you ready to start up your own comofortable business?', SH_NAME),
			'button_text' => "let's Start",
			'button_link' => '#',
			'cssclass' => ''
		), $atts ) );
		
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $button_link);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", '', $clean);
		
		return
		'<article class="focus-area bg-theme '.$cssclass.' '.$clean.'focus">
        	 <div class="container">
             	<p class="pull-left">'.$title.'</p>
            	<a href="'.$button_link.'" class="btn btn-transparent pull-right ">'.$button_text.'</a> </div>
    	</article>';
	}
	
	/*------------homepage version 2-----------------------*/
	
	function multi_purpose($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('We Create Awesoem Webdesign', SH_NAME),
			'sub_title' => __('CRAPER MULTI-PURPOSE TEMPLATE', SH_NAME),
			'imgs' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-roundabout-min', 'jquery-roundabout-shapes-min'));
		//include( get_template_directory().'/includes/modules/multi_purpose.php');
		include( _WSH()->includes( 'includes/modules/multi_purpose.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function endless_space($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			'btn_text' => '',
			'btn_link' => '',
			'btn_class' => '',
			'img' => '',
			'bg_img' => '',
			
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/endless_space.php');
		include( _WSH()->includes( 'includes/modules/endless_space.php' ) );
		
		
		wp_reset_query() ;
		return $output;

	}
	
	function endless_space_ra($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			'btn_text' => '',
			'btn_link' => '',
			'img' => '',
			'bg_img' => '',
			
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/endless_space.php');
		include( _WSH()->includes( 'includes/modules/endless_space_ra.php' ) );
		
		
		wp_reset_query() ;
		return $output;

	}
	
	function our_sponsors($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('OUR SPONSORS AND PARTNERS', SH_NAME),
			'snippet' => '',
			'num' => 16,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-carouFredSel-6-2-1-packed'));
		
		$args = array('post_type' => 'sh_clients', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		//include( get_template_directory().'/includes/modules/our_sponsors.php');
		include( _WSH()->includes( 'includes/modules/our_sponsors.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function latest_from_blog($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('LATEST TOPICS FROM THE BLOG', SH_NAME),
			'num' => 3,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		$args = array('post_type' => 'post', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat));
		query_posts($args);
		//include( get_template_directory().'/includes/modules/latest_from_blog.php');
		include( _WSH()->includes( 'includes/modules/latest_from_blog.php' ) );
		wp_reset_query() ;
		return $output;
	}
	
	function latest_from_3_categories($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('LATEST TOPICS FROM 3 CATEGORIES', SH_NAME),
			'num' => 1,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
			'cat2' => '',
			'cat3' => '',
		
		), $atts ) );
		
		$args = array('post_type' => 'post', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		$args2 = array('post_type' => 'post', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		$args3 = array('post_type' => 'post', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		
		if( $cat ) {
			$args['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat));
		}
		if( $cat2 ) {
			$args2['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat2));
		}
		if( $cat3 ) {
			$args3['tax_query'] = array(array('taxonomy' => 'category','field' => 'id','terms' => $cat3));
		}
		$category1 = query_posts($args);
		$category2 = query_posts($args2);
		$category3 = query_posts($args3);
		//include( get_template_directory().'/includes/modules/latest_from_blog.php');
		include( _WSH()->includes( 'includes/modules/latest_from_3_categories.php' ) );
		wp_reset_query() ;
		return $output;
	}	
	
	
	/*------------homepage version 3-----------------------*/
	
	function fact_buy_theme($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			'btn_text' => '',
			'btn_link' => '',
		
		), $atts ) );
		

			return
			'<div class="container">
            	 <div class="info-blocks margin-top-minus80">
                 	<div class="info-block animated out" data-animation="fadeInUp" data-delay="0">
                    	<div class="pull-left align-left">
                        	<p>'.$text.'</p>
                        	<h4>'.$title.'</h4>
                    	</div>
                    	<a href="'.$btn_link.'" class="btn btn-primary btn-icon-hold pull-right"><span>'.$btn_text.'</span><i class="icon-page-arrow-right"></i></a> 
					</div>
            	 </div>
        	</div>';
	
	}
	
	function build_your_website($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('The Easiest Way to Build Your Website', SH_NAME),
			'num' => 6,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/build_your_website.php');
		include( _WSH()->includes( 'includes/modules/build_your_website.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function product_video($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('PRODUCT INTRODUCTION VIDEO', SH_NAME),
			'sub_title' => __('Create your own homepage today and buy the full license here', SH_NAME),
			'icon_link' => '#',
			'btn_text' => __('Play Video', SH_NAME),
			'btn_link' => '#',
			'bg_img' => '',
			
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/product_video.php');
		include( _WSH()->includes( 'includes/modules/product_video.php' ) );
		
		wp_reset_query() ;
		return $output;

	}
	
	function awesome_features_list($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Some awesome features of our product', SH_NAME),
			
		), $atts ) );
		
		return '<div class="col-xs-12 ">
                        <h4>'.$title.'</h4>
                        <ol class="bullet-3">
                            '. do_shortcode( $contents ) .'
                        </ol>
                    </div>';
		return $output;
	}
	
	function awesome_feature($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			
		), $atts ) );
		

			return
			'<li class="animated out" data-animation="fadeInLeft" data-delay="0">
             	<h4>'.$title.'</h4>
                '.$text.' </li>';

	}
	
	function goodlooking_mockup($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Feel the power of goodlooking mockups', SH_NAME),
			'text' => '',
			'img1' => '',
			'img2' => '',
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/goodlooking_mockup.php');
		include( _WSH()->includes( 'includes/modules/goodlooking_mockup.php' ) );
		
		wp_reset_query() ;
		return $output;

	}
	
	function user_quicktips($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('USER INFROMATIVE QUICKTIPS', SH_NAME),
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/user_quicktips.php');
		include( _WSH()->includes( 'includes/modules/user_quicktips.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*-------------homepage version 4----------------*/
	
	function latest_projects($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Latest Projects', SH_NAME),
			'num' => 6,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
			'button_text' => 'View All Projects',
			'button_link' => '',

		), $atts ) );
		
		wp_enqueue_script(array('jquery-prettyPhoto'));
		
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/latest_projects.php');
		include( _WSH()->includes( 'includes/modules/latest_projects.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function what_we_do($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('GET AN IDEA OF WHAT WE DO', SH_NAME),
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		wp_enqueue_script(array('jquery-knob'));
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/what_we_do.php');
		include( _WSH()->includes( 'includes/modules/what_we_do.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function fun_facts_both($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'num' => 4,
		
		), $atts ) );
		
		return '<div class="section">
					<div class="focus-area bg-theme tip-btm">
						<div class="container align-center">
							<p>See what we have already done for our company!</p>
						</div>
					</div>
					<div class="container">
						<div class="row">
							'. do_shortcode( $contents ) .'
						</div>
					</div>
				</div>';
		return $output;
	}
	
	function fact_both($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'num' => '',
			'icon' => '',
			'icon_size' => '',
		
		), $atts ) );
		
			return
			'<div class="col-xs-12 col-sm-3 col-md-3 animated out" data-animation="flipInX" data-delay="0">
             	<div class="block align-center">
                	<div class="iconic iconic-large"> <i class="icon-fa icon-'.$icon.' icon-'.$icon_size.'x theme-color"></i> </div>
                    <h4 class="stat-dig">'.$num.'</h4>
                    <h5>'.$title.'</h5>
                </div>
             </div>';

	}
	
	function team($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Meet our Successful Team', SH_NAME),
			'num' => 3,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
			'btn_link' => '',
			'btn_text' => '',
			'hiring_text' => '',
			'show_hiring'=> false,
			

		), $atts ) );
		
		$args = array('post_type' => 'sh_team', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'team_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/team.php');
		include( _WSH()->includes( 'includes/modules/team.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*----------------homepage version 5-----------------------*/
	
	function single_portfolio($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('PROJECT SINGLE ITEM', SH_NAME),
			'num' => 1,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/single_portfolio.php');
		include( _WSH()->includes( 'includes/modules/single_portfolio.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function work_with_us($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('WHY YOU SHOULD WORK WITH US', SH_NAME),
			'num' => 3,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/work_with_us.php');
		include( _WSH()->includes( 'includes/modules/work_with_us.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function blue_ribbon($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('Take A Look At Our Plans And Pricing Tables', SH_NAME),
			
		), $atts ) );
		
		return
		'<div class="section no-padding-top">
            	<div class="focus-area bg-theme tip-btm no-margin-top">
                	<div class="container align-center">
                    	<p>'.$title.'</p>
                	</div>
            	</div>
			</div>';
	}
	
	function ptable_container( $atts, $contents = null )
	{
		extract( shortcode_atts( array(
			'column' => '',
			
		), $atts ) );
		
		$view = 0;
		
		//include( get_template_directory().'/includes/modules/pricing_table.php');
		include( _WSH()->includes( 'includes/modules/pricing_table.php' ) );
		
		return $output;
		
	}
	
	function ptable( $atts, $contents = null )
	{
		extract( shortcode_atts( array(
			'title' => __('Standard', SH_NAME),
			'desc' => '',
			'price' => '',
			'btn' => __('Order Now', SH_NAME),
			'btn_link' => '',
			'featured' => false,
			
		), $atts ) );
		
		$view = 1;
		
		//include( get_template_directory().'/includes/modules/pricing_table.php');
		include( _WSH()->includes( 'includes/modules/pricing_table.php' ) );
		
		return $output;
	}
	
	function ptable_features( $atts, $contents = null )
	{
		extract( shortcode_atts( array(
			'title' => __('Standard', SH_NAME),
			
		), $atts ) );
		
		$view = 2;
		
		//include( get_template_directory().'/includes/modules/pricing_table.php');
		include( _WSH()->includes( 'includes/modules/pricing_table.php' ) );
		
		return $output;
		
	}
	
	function portfolio_full_width($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'desc' => __('Our Portfolio fullwidth', SH_NAME),
			'num' => 3,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-mixitup-min', 'jquery-prettyPhoto'));
		
		$paged = get_query_var('paged');
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order, 'paged'=>$paged);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/portfolio_inner.php');
		include( _WSH()->includes( 'includes/modules/portfolio_inner.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function tweet_check_out($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'twitter_id' => 'wowtheme',
			'number' => 3
			
		), $atts ) );
		
		wp_enqueue_script(array('jquery-carouFredSel-6-2-1-packed'));
		
		FW_Twitter(array('Template'=>'p','screen_name'=>$twitter_id, 'count'=>$number, 'selector'=>'.tweets-shortcode'));
		
		return
		'<article class="focus-area bg-theme">
			<div class="container">
				<div class="tweets-shortcode"></div>
			</div>
		</article>';
	}
	
	/*--------------portfolio with grid width---------------*/
	
	function portfolio_grid_width($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'desc' => __('Our Portfolio Grid width', SH_NAME),
			'num'	  => 10,
			'orderby'  => 'date',
			'order'	=> 'DESC',
			'cat'	  => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-mixitup-min', 'jquery-prettyPhoto'));
		
		$paged = get_query_var('paged');
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order, 'paged'=>$paged);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/portfolio_grid_width.php');
		include( _WSH()->includes( 'includes/modules/portfolio_grid_width.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*--------------portfolio with grid three---------------*/
	
	function portfolio_grid_three($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'desc'     => __('Our Portfolio Grid width', SH_NAME),
			'num'	  => 10,
			'orderby'  => 'date',
			'order'	=> 'DESC',
			'cat'	  => '',
			
		), $atts ) );
		
		wp_enqueue_script(array('jquery-mixitup-min', 'jquery-prettyPhoto'));
		
		$paged = get_query_var('paged');
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order, 'paged'=>$paged);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/portfolio_grid_three.php');
		include( _WSH()->includes( 'includes/modules/portfolio_grid_three.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*--------------portfolio with grid four---------------*/
	
	function portfolio_grid_four($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'desc'     => __('Our Portfolio Grid width', SH_NAME),
			'num'	  => 10,
			'orderby'  => 'date',
			'order'	=> 'DESC',
			'cat'	  => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-mixitup-min', 'jquery-prettyPhoto'));
		
		$paged = get_query_var('paged');
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order, 'paged'=>$paged);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/portfolio_grid_four.php');
		include( _WSH()->includes( 'includes/modules/portfolio_grid_four.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*--------------Portfolio with Sidebar---------------*/
	
	function portfolio_with_sidebar($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'desc' => __('Our Portfolio Grid width', SH_NAME),
			'column' => 'col-2',
			'num' => 3,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-mixitup-min', 'jquery-prettyPhoto'));
		
		$paged = get_query_var('paged');
		$args = array('post_type' => 'sh_portfolio', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order, 'paged'=>$paged);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
		query_posts($args);
		
		//include( get_template_directory().'/includes/modules/portfolio_with_sidebar.php');
		include( _WSH()->includes( 'includes/modules/portfolio_with_sidebar.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*--------------About us page---------------*/
	
	function office($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('This is our wounderful office', SH_NAME),
			'city' => __('New York - Albany', SH_NAME),
			'text' => '',
			'btn_text' => __('Find us on the Map', SH_NAME),
			'btn_link' => '#',
			'img' => '',
		
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/office.php');
		include( _WSH()->includes( 'includes/modules/office.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function blockquote($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array( 'jquery-carouFredSel-6-2-1-packed' ));
		$args = array('post_type' => 'sh_testimonial', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'testimonial_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/blockquote.php');
		include( _WSH()->includes( 'includes/modules/blockquote.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function fun_facts_both_parallex($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'img' => '',
		
		), $atts ) );
		
		return 
		'<article class="parallex-section" style="background-image:url('.wp_get_attachment_url( $img, 'full' ).');">
			<div class="container">
				<div class="row">
					'. do_shortcode( $contents ) .'
				</div>
			</div>
		</article>';
		return $output;
	}
	
	function fact_both_parallex($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'btn_text' => '',
			'btn_link' => '',
			'num' => '',
			'icon' => '',
			'icon_size' => '',
		
		), $atts ) );
		
			return
			'<div class="col-xs-12 col-sm-3 col-md-3 animated out" data-animation="fadeIn" data-delay="0">
             	<div class="block align-center">
                	<div class="iconic iconic-large"> <i class="icon-fa icon-'.$icon.' icon-'.$icon_size.'x theme-color"></i> </div>
                        <h4 class="stat-dig">'.$num.'</h4>
                        <a href="'.$btn_link.'" class="btn btn-trans-border">'.$btn_text.'</a> 
				</div>
             </div>';

	}
	
	function normal_text_box($atts) {
		extract( shortcode_atts( array(
			'title' => __('TEXT BOX', SH_NAME),
			'text' => ''
		), $atts ) );
		
		include( _WSH()->includes( 'includes/modules/normal_text_box.php' ) );
		return $output;
	}
	
	
	/*---------------services------------------------*/
	
	function we_do_for_you($atts, $contents = null)
	{
			global $post;
		extract( shortcode_atts( array(
			'title' => __('WHAT WE DO FOR YOU', SH_NAME),
			'num' => 6,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$post_slug = $post->post_name;
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/what_we_do_you.php');
		include( _WSH()->includes( 'includes/modules/what_we_do_you.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function working_process($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => __('EASY WORKING PROCESS', SH_NAME),
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',

		), $atts ) );
		
		wp_enqueue_script(array('jquery-knob'));
		
		$args = array('post_type' => 'sh_service', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'service_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		
		//include( get_template_directory().'/includes/modules/working_process.php');
		include( _WSH()->includes( 'includes/modules/working_process.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	/*-------------------------shortcodes-----------------------*/
	
	function button($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'btn_text' => '',
			'btn_link' => '',
			'btn_style' => 'btn-primary',
			'btn_size' => 'btn-large',

		), $atts ) );
		
		
		//include( get_template_directory().'/includes/modules/button.php');
		include( _WSH()->includes( 'includes/modules/button.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function button_with_icon($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'btn_text' => '',
			'btn_link' => '',
			'btn_style' => 'btn-primary',
			'btn_size' => 'btn-large',
			'btn_icon' => 'left',
			

		), $atts ) );
		
		
		//include( get_template_directory().'/includes/modules/button_with_icon.php');
		include( _WSH()->includes( 'includes/modules/button_with_icon.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function alert($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'msg' => '',
			'alert_style' => 'alert-success',
			
			
		), $atts ) );
		
		
		//include( get_template_directory().'/includes/modules/alert.php');
		include( _WSH()->includes( 'includes/modules/alert.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function blockquote_half($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'num' => 4,
			'orderby' => 'date',
			'order' => 'ASC',
			'cat' => '',
		
		), $atts ) );
		
		wp_enqueue_script(array('jquery-carouFredSel-6-2-1-packed'));
		
		$args = array('post_type' => 'sh_testimonial', 'showposts'=>$num, 'orderby'=>$orderby, 'order'=>$order);
		if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'testimonial_category','field' => 'id','terms' => $cat));
		query_posts($args);
		$services = 1;
		//include( get_template_directory().'/includes/modules/blockquote_half.php');
		include( _WSH()->includes( 'includes/modules/blockquote_half.php' ) );
		
		wp_reset_query() ;
		return $output;
	}
	
	function fact_buy_theme_half($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'text' => '',
			'btn_text' => '',
			'btn_link' => '',
		
		), $atts ) );
		

			return
			'<div class="col-xs-12">
			 	<div class="info-blocks bg-transparent">
			 		<div class="info-block">
						<div>
							<p>'.$text.'</p>
							<h4>'.$title.'</h4>
						</div>
						<a href="'.$btn_link.'" class="btn btn-primary btn-icon-hold"><span>'.$btn_text.'</span><i class="icon-page-arrow-right"></i></a> 
					</div>
				</div>
			</div>';
	
	}
	
	function office_address($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => '',
			'address' => '',
			'telephone' => '',
			'email' => ''
		
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/get_in_touch.php');
		include( _WSH()->includes( 'includes/modules/office_address.php' ) );
		
		wp_reset_query() ;
		return $output;
	}

	function Get_in_touch($atts, $contents = null)
	{
		extract( shortcode_atts( array(
			'title' => 4,
			'text' => 'date',
		
		), $atts ) );
		
		//include( get_template_directory().'/includes/modules/get_in_touch.php');
		include( _WSH()->includes( 'includes/modules/get_in_touch.php' ) );
		
		wp_reset_query() ;
		return $output;
	}

	function excerpt( $text, $len = 35 )
	{
		$text = strip_shortcodes( $text );
	
		$text = apply_filters( 'the_content', $text );
		$text = str_replace(']]>', ']]&gt;', $text);
		
		$excerpt_length = apply_filters( 'excerpt_length', $len );
		
		$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
		
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
		
		return $text;
	}
	

	
}
?>