<?php global $wp_query;
$t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php if( have_posts()):?>
<div class="col-xs-12 animated margin-bottom out" data-animation="fadeInRight" data-delay="400">
	<div class="client-logos caro-1-col">
		<ul class="caro-slider-ul">
			
			<li>
				<?php $count=0;?>
				<?php while( have_posts()): the_post(); ?>
				
					<?php if(($count%4) == 0 && $count != 0):?>
						</li><li>
					<?php endif; ?>
					
					<div class="client-logo"> <a href="<?php the_permalink();?>"><?php the_post_thumbnail('230x230');?></a> </div>
				
				<?php $count++; endwhile; ?>
		
			</li>
			
		</ul>
		<div class="caro-controls controls-right"> <a href="#" class="caro-prev"></a> <a href="#" class="caro-next"></a> </div>
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>