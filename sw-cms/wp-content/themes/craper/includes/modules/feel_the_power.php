<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<div class="col-xs-12">
	 <div class="display">
		<h4><?php echo $title;?></h4>
		<p><?php echo $text;?></p>
		<figure class="animated out" data-animation="fadeInUpBig" data-delay="0">  <?php echo wp_get_attachment_image( $img, 'full' ); ?> </figure>
	 </div>
</div>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>