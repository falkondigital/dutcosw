<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="display">
					<figure class="image"> <?php echo wp_get_attachment_image( $img, 'full' ); ?> </figure>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<header>
					<h2><a href="#"><?php echo $title;?></a></h2>
				</header>
				<p class="meta"><?php echo $city;?></p>
				<p><?php echo $text;?></p>
				<a href="<?php echo $btn_link;?>" class="btn btn-primary"><?php echo $btn_text;?></a> </div>
		</div>
	</div>
</div>
<!-- /section -->

<?php  

$output = ob_get_contents();
ob_end_clean();

?>