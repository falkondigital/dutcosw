<?php global $wp_query;
$t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php if( have_posts()):?>
<div class="section">
<div class="clients-area">
	<div class="container">
		<header class="heading">
			<h2><?php echo $title;?></h2>
			<?php echo ($snippet != '') ? '<p>'.$snippet.'</p>' : ''?>
		</header>
		<div class="client-logos-full caro-1-col">
			<ul class="caro-slider-ul">
				<li>
					<?php $count=0;?>
					<?php while( have_posts()): the_post(); ?>
					
					<?php if(($count%8) == 0 && $count != 0):?>
						</li><li>
					<?php endif; ?>
					
					<div class="client-logo"> <a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a> </div>
					
					<?php $count++; endwhile; ?>
					
				</li>
			</ul>
			<div class="caro-controls controls-right"> <a href="#" class="caro-prev"></a> <a href="#" class="caro-next"></a> </div>
		</div>
	</div>
</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>