<?php $t = $GLOBALS['_sh_base'];
ob_start();

?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section no-padding-bottom">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title;?></h2>
		</header>
		<div class="row">

			<?php $i = 0; while( have_posts()): the_post();
			if ($post_slug != 'advisory-services' && $i!= 0 && ($i)%3===0) { echo '<div style="width:100%; display:block; float:left;"></div>';}
			$thispost = get_post(get_the_id());
			
							$meta = get_post_meta( get_the_id(), '_sh_services_meta', true );//printr($meta);?>

			<div class="col-xs-12 col-sm-4 <?php echo ($post_slug == 'advisory-services') ? 'col-md-5row' : 'col-md-4' ?>  animated out" data-animation="fadeIn" data-delay="<?php echo $delay;?>">
				<div class="block horizontal-icon">
					<div class="iconic theme-color"> <i class="icon-fa <?php echo ($post_slug == 'advisory-services') ? 'icon-tick' : 'icon-'.$thispost->post_name ;?> <?php //echo sh_set($meta, 'font_awesome_icon');?> icon-4x"></i> </div>
					<h4><?php if ($post_slug != 'advisory-services') { ?><a href="<?php the_permalink();?>"><?php the_title();?></a><?php } else { ?><?php the_title();?><?php } ?></h4>
					<p><?php echo $this->excerpt(get_the_excerpt(), 160); ?></p>
				</div>
				<!-- /block --> 
			</div>
		
			<?php $i++; $delay+=200; endwhile; ?>
		
		</div>
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>