<?php get_header(); 
$meta = get_post_meta( get_the_id(), '_craper_post_settings', true); 
$sidebar = sh_set( $meta, 'post_sidebar' );
$layout = sh_set($meta, 'layout');
?>
<article class="banner">
	<div class="container">
		<h2><?php the_title();?></h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
				<?php //echo get_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</article>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article role="main">
	<div class="section">
		<div class="container">
			<div class="row">
			
					<?php $class = ( $layout == 'full' ) ? 'col-xs-12 col-sm-8 col-md-12' : 'col-xs-12 col-sm-8 col-md-8';
						if( $sidebar && $layout == 'left' ): ?>
							<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
								<?php dynamic_sidebar( $sidebar ); ?>
							</div>
					<?php endif; ?>
			
				<div id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
					<div class="contents">
						<div class="blog-box">
							<?php $no_image = ( !has_post_thumbnail() ) ? ' no-image' : ''; ?>
							<!--<figure class="image image-hovered animated out<?php echo $no_image; ?>" data-animation="fadeInUp" data-delay="0"> <?php the_post_thumbnail(array('300','200'));?>
								<figcaption>
									<h5><?php the_title();?></h5>
									<p><?php echo get_the_term_list(get_the_id(), 'category', '', ', '); ?></p>
								</figcaption>
							</figure>-->
							<header class="clearfix">
								<h3 class="pull-left"><a href="<?php the_permalink();?>"><?php the_time( get_option( 'date_format' ) ); ?></a></h3>
								<ul class="options pull-right">
									<li><a href="<?php the_permalink();?>"><i class="icon-chat-01"></i><?php comments_number('0', '1', '%' );?> comments</a></li>
								</ul>
							</header>
							<p class="meta">posted on <?php the_time( get_option( 'date_format' ) ); ?> in <?php echo get_the_term_list(get_the_id(), 'category', '', ', '); ?></p>
							<?php the_post_thumbnail();?>
							<?php the_content();?>
							<?php wp_link_pages(array('before'=>'<div class="pagination">'.__('Pages: ', SH_NAME), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
							<div class="share-bar">
								<div class="addthis_toolbox addthis_default_style">
									<p><?php _e('Share this post', SH_NAME); ?></p>
									<ul>
										<li><a class="addthis_button_tweet"></a></li>
										<li><a id="plusone" class="addthis_button_google_plusone"></a></li>
										<li><a id="facebookLike" class="addthis_button_facebook_like"></a></li>
										<li><a class="addthis_counter addthis_pill_style"></a></li>
									</ul>
								</div>
							</div>
							<!-- /share-bar --> 
						</div>
						
						<?php comments_template(); ?>
						
					</div>
				</div>
				
				<?php if( $sidebar && $layout == 'right' ): ?>
					<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
				<?php endif; ?>
				
			</div>
		</div>
	</div>
</article>

<?php endwhile; endif; ?>
	
<?php get_footer(); ?>