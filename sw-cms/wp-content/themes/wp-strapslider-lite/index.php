<?php
/**
 *
 * Description: Default Index template to display loop of blog posts
 *
 */

get_header(); ?>
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="row-fluid">

<!-- Start Of Our Posts -->
    <div class="span8">
    <?php get_template_part('templates/content', get_post_format()); ?>
	</div>
<!-- End Of Our Posts Section -->

<!-- Insert Our Sidebar -->
    <div class="span4">
    <?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
	            <?php dynamic_sidebar( 'sidebar-blog' ); ?>
            <?php endif; // end sidebar widget area ?> 
	</div>
<!-- End the sidebar -->

</div><!-- /.row --> 

<?php get_footer(); ?>