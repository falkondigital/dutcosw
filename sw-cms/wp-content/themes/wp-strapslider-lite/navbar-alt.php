<!-- NAVBAR
    ================================================== -->
    <div class="navbar-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container">

        <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <div class="span3">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
			<p class="site-description"><?php bloginfo( 'description' ); ?></p>
			<!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            </div>
			<div class="span8 pull-right">
			<div class="nav-collapse collapse">
            <!-- Our menu needs to go here -->
			<?php wp_nav_menu( array(
	           'theme_location'	=> 'main-menu',
	           'menu_class'		=>	'nav nav-pills',
	           'depth'			=>	0,
	           'fallback_cb'	=>	false,
	           'walker'			=>	new WPStrapSlider_Nav_Walker,
	           )); 
            ?>
			</div><!--/.nav-collapse -->
			</div>
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->