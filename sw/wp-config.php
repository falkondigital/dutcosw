<?php
/** Enable W3 Total Cache Edge Mode */
define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

ini_set('memory_limit', '-1');
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define('DB_NAME', 'swdutco_db');

/** MySQL database username */
define('DB_USER', 'swdutco_user');

/** MySQL database password */
define('DB_PASSWORD', '!70m+I=3A&ww');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8pi8E0v9b16i8p6M9ryu3awmL4zbwV4THY5kh8Dq2MxwuAysbP50mDrEGvjBLuIw');
define('SECURE_AUTH_KEY',  'B9XwDRjCHHUWCPmq0ETe50LrVUzmYxZ1WJ8yS5FjUo63gHGUjW3GWB1XZ3ctYwVZ');
define('LOGGED_IN_KEY',    '_CiInRa7K1WFKXOj9OYShyScriuETXApS9RAMfNAc9dr22Gn6fDNVW5Nl5FCUyaW');
define('NONCE_KEY',        '7rXZSvG4BE2269sgqZJsaFpPcmpNGNKY2WFvc0_hiRWyn576SEmQk4FMOd26KOFx');
define('AUTH_SALT',        'r9It37G8bwF9ybWfie8dN0agw3KyuxEx8baCmTssCaA7ug6E_Ec6B9ZqTcuBI8Oo');
define('SECURE_AUTH_SALT', 'KzzalaFzFRjVc1FPfi9MpvqiGBaNQctNqq3GladcXQu68LQ46AkzLZ9PLtqew6DV');
define('LOGGED_IN_SALT',   '1v84fKBj6wZlGFfpFd_wUTMjcjPgKeH8hqfWzScD1stgMdZtagHongl9i51v6GAT');
define('NONCE_SALT',       'a6DxVLqRneqf1ZDws5Uf1J37SXrLT4ynAT60W6EqDKJddiHQDM7e1bKx3PKmbENH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sw_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');