"�X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:346;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2013-10-01 15:34:15";s:13:"post_date_gmt";s:19:"2013-10-01 15:34:15";s:12:"post_content";s:1486:"[vc_row][vc_column width="1/1"][layerslider_vc id="20"][/vc_column][/vc_row][vc_row][vc_column width="3/4" el_class="infoBox"][vc_column_text el_class="textBox"]
<h1>Experts in Design and Build</h1>
Our complementary expertise in business process engineering and property management systems enables us to deliver an integrated range of support services throughout a property’s life cycle.

Teaming up with Architects and Interior Design Consultants allows projects to have different look and feel, rather than producing very similar end projects.

We have an exceptional track record in design co-ordination, either working alongside existing concept designers to implement their projects, or providing all services from project inception onwards. We are able to contribute and add value to the transition from an early stage and strive to honestly deliver the design intent whilst dealing with the key factors commonly encountered, including:

&nbsp;
<ul>
	<li>Obtaining permissions from Civil Defense, Municipality, Utilities and Landlords.</li>
	<li>Sustainable procurement of high quality materials</li>
	<li>Budgetary control</li>
	<li>Service co-ordination and distribution</li>
	<li>Phasing of works to keep buildings “live”</li>
	<li>Critical timelines</li>
	<li>Developments in scope</li>
</ul>
[/vc_column_text][/vc_column][vc_column width="1/4" el_class="infoBox"][vc_widget_sidebar sidebar_id="defaultsidebar" el_class="textBox"][/vc_column][/vc_row]";s:10:"post_title";s:27:"Experts in Design and Build";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:27:"experts-in-design-and-build";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2013-12-17 18:37:44";s:17:"post_modified_gmt";s:19:"2013-12-17 18:37:44";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:101;s:4:"guid";s:31:"http://dutcosw.com/?page_id=346";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}