�Q�X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:2:"27";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2013-06-12 10:40:25";s:13:"post_date_gmt";s:19:"2013-06-12 10:40:25";s:12:"post_content";s:14581:"/*© Copyright - Mark Firth 2014 - This code is subject to IPR law*/
/*iSite Framework - Designed and coded by Mark Firth*/
h1 {
	font-size: 140% !important;
}

h2 {
	font-size: 100% !important;
}

h3 {
	font-size: 95% !important;
}

#footer h3 {
	color: #ddd !important;
}

strong, textBox strong {
	color: #888 !important;
	font-weight: 400 !important;
}

h3 a:hover {
	color: #666 !important;
}

h3 a {
	color: #0080BC !important;
}

.infoBox h1, .infoBox h2, .infoBox h3, .infoBox h4, .infoBox h5, .infoBox h6 {
	text-transform: uppercase !important;
	color: #777 !important;
}

/*POST FEATURED IMAGE*/
.zn_post_image {
	width: auto !important;
}

/*FORMS*/
.gform_wrapper {
	max-width: 100% !important;
}

.swEmail input, .gform_wrapper select, .gform_wrapper textarea {
	font-size: 100% !important;
	padding: 5px !important;
	font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	color: #666 !important;
}

.gform_wrapper textarea {
	width: 98% !important;
}

.gform_wrapper select {
	padding: 3px !important;
}

.gform_wrapper .gfield_required {
	display: none !important;
}

.swEmail .gform_footer input[type="submit"] {
	font-size: 1.1em !important;
	color: #777 !important;
/*width: 100px !important;*/
	padding: 3px 12px !important;
	text-transform: uppercase !important;
}

.mySpacer {
	margin-bottom: 16px !important;
}

.flexslider {
	margin: 0 0 15px !important;
}

.hideMe {
	display: none;
}

section#content {
	margin-top: 30px !important;
}

/*NEWS*/
.showbiz .divide20 {
	height: 8px !important;
}

#showbiz_news_1 .showbiz-title, #showbiz_news_1 a.showbiz-title, #showbiz_news_1 a.showbiz-title:visited {
	font-size: 14px !important;
	line-height: 14px !important;
/*NEWS END*/
}

/*.infoBoxFull {
	border: 1px solid #fff;
	padding: 16px;
	background: #ececec;
	background: rgba(236,227,212,0.21);
margin-left: -25px !important;
}

.boxFix {
	margin-right: -10px !important;
	margin-left: 21px !important;
}

body.res1170 [class*="span"] {
	margin-left: 10px !important;
}
*/
.infoBox {
	border: 1px solid #fff;
	padding: 16px !important;
	background: #ececec;
	background: rgba(236,227,212,0.21);
	margin-bottom: 6px !important;
/*margin-left: 8px !important;*/
}

/*.infoBox.vc_span6 {
	width: 49% !important;
	margin: 1% !important;
}*/
.infoBox p {
	font-size: 90% !important;
}

.infoBox a {
	color: blue !important;
}

.infoBox a:hover {
	text-decoration: underline !important;
}

.infoBox ul li a {
	color: #666 !important;
}

.infoBox .title a {
	color: #666 !important;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-size: 80% !important;
}

.infoBox .text {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-size: 70% !important;
}

.infoBox .btn {
	color: #333 !important;
	text-decoration: none !important;
}

.infoBox .btn:hover {
	color: #333 !important;
	text-decoration: none !important;
}

.infoBox .btn-success {
	color: #FFF !important;
	text-decoration: none !important;
}

.infoBox .btn-success:hover {
	color: #FFF !important;
	text-decoration: none !important;
}

.infoBox:hover {
	background: #E9E9E9;
}

.infoBox .widget {
	margin-bottom: 5px !important;
}

.textBox, .iconHolder, .infoBox {
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}

.textBox {
	padding: 12px !important;
	background: #f5f5f5 url('http://www.stylesandwood-group.co.uk/sw-cms/wp-content/uploads/striped-bg2.png') repeat;
}

.infoBox .textBox {
/*margin-bottom: 8px !important;*/
}

.textBox, .iconHolder {
	-webkit-box-shadow: 1px 1px 2px rgba(75,75,75,0.07);
	-moz-box-shadow: 1px 1px 2px rgba(75,75,75,0.07);
	box-shadow: 1px 1px 2px rgba(75,75,75,0.07);
	border: 1px solid #e5e5e5;
}

.textBox h1, .textBox h2, .textBox h3, .textBox h4, .textBox h5, .textBox h6 {
	margin-top: 0 !important;
}

.textBoxDark {
	padding: 12px !important;
	margin-bottom: 16px !important;
	-webkit-box-shadow: 1px 1px 2px rgba(75,75,75,0.07);
	-moz-box-shadow: 1px 1px 2px rgba(75,75,75,0.07);
	box-shadow: 1px 1px 2px rgba(75,75,75,0.07);
	border: 1px solid #e5e5e5;
	background: #f5f5f5 url('http://www.stylesandwood-group.co.uk/sw-cms/wp-content/uploads/TextBoxBGDrk1.png') repeat;
}

.iconHolder {
	background: #fff;
	margin-bottom: 16px !important;
	padding-left: 16px;
	padding-top: 16px;
}

.pageIcons img {
	-webkit-transition: all .3s ease-in-out;
	-o-transition: all .3s ease-in-out;
	transition: all .3s ease-in-out;
}

.pageIcons img:hover {
	margin-top: -7px !important;
	box-shadow: 0 0 0 5px #666 inset;
}

.wpb_separator {
	padding-top: 10px !important;
	padding-bottom: 10px !important;
}

.sidebar .widget .title:after {
	border: none !important;
}

#sidebar .text, .sidebarBox .text {
	font-size: .8em !important;
}

#sidebar, .sidebarBox h3 {
	font-size: 1.1em !important;
}

#sidebar, .sidebarBox {
	border: 1px solid #fff !important;
	padding: 16px !important;
	background: #eee !important;
	background: rgba(236,227,212,0.21) !important;
	margin-left: 27px !important;
}

.myfFix {
	margin-top: 30px !important;
}

.SWNavBar {
	margin-top: 20px !important;
}

/*header*/
#header {
	z-index: 99999 !important;
}

/*Cookie Control Styling*/
.ccc-inner h2 {
	background: none !important;
	color: #666 !important;
}

.ccc-outer {
	background: none repeat scroll 0 0 #FFFFFF !important;
	border: 1px solid #BBBBBB !important;
	box-shadow: 0 0 5px rgba(0,0,0,0.3) !important;
}

.ccc-close {
	background: url('http://www.stylesandwood-group.co.uk/sw-cms/wp-content/uploads/close.jpg') !important;
	background-position: 0 -17px !important;
	border: 0 none !important;
	position: absolute !important;
	right: 20px !important;
	text-indent: -999em !important;
	top: 25px !important;
	width: 17px !important;
	height: 17px !important;
}

/*SAHRE PRICE*/
.SPG {
	margin-top: 10px;
	margin-bottom: 10px;
	margin-right: 20px;
}

.spgBox {
	width: 98%;
	height: auto;
	padding: 6px 6px 12px 12px;
	border: 1px solid #CCC;
	background: rgba(121,100,77,0.09);
	margin-bottom: 10px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-size: 100% !important;
}

.spgBoxF {
/*float: right !important;*/
	width: 73%;
	height: auto;
	padding: 0 0 4px 10px;
	border: 1px solid #666;
	background: rgba(121,100,77,0.09);
	margin-bottom: 4px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-size: 100% !important;
}

.spgBoxF, .spgBox {
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

.SPG h2 {
	color: red;
	font-weight: bold;
	font-size: .9em;
	margin: 4px 0;
}

.spgBox:hover {
	text-decoration: none !important;
	color: #FFF;
}

.SWprice {
	font-size: 110% !important;
	color: green;
	vertical-align: -10px;
}

.SPTxt {
	position: relative;
	vertical-align: -10px;
	font-size: 80%;
	color: #999;
}

.SPG a, .SPG a:hover {
	text-decoration: none;
}

.spgBox a:hover, .SPG a:hover, .SPTxt a:hover {
	text-decoration: none !important;
	color: #FFF !important;
}

/*PORTFOLIO*/
.seemore {
	display: none !important;
}

.hg-portfolio-carousel h3 a {
	color: #666 !important;
	font-size: 1.5em !important;
}

/*LAYER SLIDER*/
.ls-container {
/*margin-top: -25px !important;*/
	margin-bottom: 35px !important;
}

.ls-container h2 {
	font-size: 130% !important;
}

.projects h2 {
	font-size: 135% !important;
	color: #555 !important;
}

.projects strong {
	color: #555 !important;
}

.projects p {
	font-size: 95% !important;
	color: #777;
}

.myDownloads {
	color: #1d74d7 !important;
	padding-top: 110px !important;
	margin-top: 30px !important;
	background: url(http://dutcosw.com/sw/wp-content/uploads/downloads-2-icon.png) no-repeat;
	height: 92px;
}

.myDownloads p {
	color: #1d74d7 !important;
}

/*SEARCH BUTTON*/
.icon-search:before {
	content: "" !important;
}

/*Mega Menu*/
#megaMenu ul.megaMenu li.menu-item.ss-nav-menu-mega.mega-colgroup>ul>li {
	padding-left: 1% !important;
	padding-right: 1% !important;
}

#megaMenu ul li.menu-item.ss-nav-menu-mega ul li.menu-item.ss-nav-menu-item-depth-1 {
	padding-right: 0 !important;
	padding-left: 17px !important;
}

#megaMenu ul.megaMenu > li.menu-item > a {
	padding: 8px 15px !important;
}

/*#megaMenu ul li a img (max-width:100%!important)*/
#megaMenu {
}

#megaMenu.active {
	background: #555 !important;
	color: #fff !important;
}

/*Mega Menu End*/
/*MAIN MENU*/
nav#main_menu > ul > li.active > a, nav#main_menu > ul > li > a:hover, nav#main_menu > ul > li:hover > a, nav#main_menu > ul > li > a {
/*padding: 6px 6px 5px;*/
	margin: -6px 0 0;
	margin-left: 1px !important;
	margin-right: 1px !important;
}

nav#main_menu > ul ul li a:hover {
	color: #666 !important;
}

#menu-sw-sub a {
	font-size: 1em !important;
	color: #333 !important;
	text-transform: uppercase !important;
}

#main_menu a {
	font-size: 75% !important;
	margin-left: 0 !important;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}

#main_menu {
	text-transform: uppercase !important;
	float: left !important;
	clear: both !important;
	position: relative;
	top: -43px !important;
/*margin-left: 12px !important;*/
}

nav#main_menu > ul > li.active > a, nav#main_menu > ul > li a:hover {
	color: #fff !important;
}

/*Menu*/
/*Mobile Menu*/
#menu-sw-main {
	z-index: 999999 !important;
}

.zn_menu_trigger {
	background: #555 !important;
	position: relative !important;
	top: 20px !important;
}

.zn_menu_trigger a {
	color: #eee !important;
}

.zn_menu_trigger a:before {
	border-color: #eee !important;
	content: '';
	position: absolute;
	top: 37%;
	left: 0;
	width: .75em;
	height: .125em;
	border-top: .3em double #666;
	border-bottom: .125em solid #666;
}

.zn_menu_trigger a, .zn_menu_trigger a:hover {
	position: relative;
	padding-left: 1.2em;
	color: #666;
	font-size: 14px;
	font-weight: 700;
}

/*Accordian Header*/
.wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header {
	background: none !important;
}

.wpb_content_element .wpb_accordion_header a {
	padding: 0 !important;
	border: none !important;
}

.mySplitterFix {
	margin-top: 45px !important;
}

:focus {
	outline: -webkit-focus-ring-color none !important;
}

.wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header h2 {
	color: #666 !important;
	margin-top: 10px !important;
	margin-bottom: 10px !important;
	margin-left: 30px !important;
}

.wpb_accordion .wpb_accordion_wrapper .ui-state-default .ui-icon, .wpb_accordion .wpb_accordion_wrapper .ui-state-active .ui-icon {
	left: 0 !important;
}

.zn_def_header_style {
	min-height: 90px !important;
	background: #f5f5f5 !important;
}

#page_header {
	height: 90px !important;
	border-bottom: 1px solid #ccc;
	-webkit-box-shadow: 0 1px 10px 0 #666;
	box-shadow: 0 1px 10px 0 #666;
	margin-bottom: 15px !important;
	min-height: none !important;
}

.wpb_row ul.wpb_thumbnails-fluid > [class*="vc_span"] {
	background: rgba(218,218,218,0.81)url('/sw-cms/wp-content/uploads/leather.png') repeat;
	padding-left: 15px !important;
	padding-right: 15px !important;
	border: solid 1px #999 !important;
	margin-right: 12px !important;
	margin-bottom: 18px !important;
	-moz-box-shadow: 0 0 9px 0 #777 !important;
	-webkit-box-shadow: 0 0 9px 0 #777;
	box-shadow: 0 0 9px 0 #777;
}

.wpb_row ul.wpb_thumbnails-fluid > [class*="vc_span"] h2 {
	font-size: 1.5em !important;
	line-height: 120% !important;
}

.wpb_row ul.wpb_thumbnails-fluid > [class*="vc_span"] h2 a {
	color: #666 !important;
}

/*Header*/
#logo {
	position: relative !important;
	float: right !important;
	clear: left !important;
	margin: 0 !important;
}

#logo a img {
	margin-top: 15px !important;
	max-width: none !important;
	width: 172px !important;
	height: 63px !important;
}

#header.style3 #logo a {
	border: none !important;
	background: none !important;
	padding: 0 !important;
/*padding-top: 25px !important;*/
	background: transparent !important;
	-ms-filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#F5F5F5,endColorstr=#F5F5F5);
/* IE8 */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#F5F5F5,endColorstr=#F5F5F5);
/* IE6 & 7 */
	zoom: 1 !important;
	background: transparent !important;
	min-height: none !important;
	padding: 0 !important;
	line-height: 0 !important;
	text-align: right !important;
	height: none !important;
}

#header.style3 #logo a:after {
	border: none !important;
}

#header {
	position: absolute !important;
/*height: auto!important;*/
}

#header #bar {
	display: none;
	position: absolute;
	z-index: 1;
	top: 40px;
	left: 250px;
}

/*Slider thumbs*/
.ls-noskin .ls-thumbnail {
	top: -40px !important;
	left: 7px !important;
	float: left !important;
}

#arLink p:first-of-type {
	margin-top: -20px;
}

#arTxt {
	position: absolute;
	color: red;
	left: 20px;
	top: 325px;
	z-index: 100000;
}

#clickTxt {
	font-weight: bold;
	position: relative;
	left: 20px;
	top: 350px;
	background: #151515;
	color: #888;
	padding: 0 10px;
	z-index: 100000;
}

#clickTxt:hover {
	padding: 0 10px;
	background: #222;
	color: #ccc;
}

/*List Effect*/
.listArrow {
	-webkit-transition: all .3s ease-in-out;
	-o-transition: all .3s ease-in-out;
	transition: all .3s ease-in-out;
	margin-top: 7px;
	margin-bottom: 0;
	background: url(/uploads/2012/06/arrow_box_icon1.gif) no-repeat;
	border-bottom: 1px solid #DDD;
	border-top-width: 1px;
	border-right-width: 1px;
	height: 22px;
	padding-left: 20px;
	width: 80%;
}

.listArrow a:link {
	color: #777 !important;
}

.listArrow a:hover {
	text-decoration: none;
	color: #F00 !important;
}

.listArrow a:visited {
	text-decoration: none;
	color: #CCC !important;
}

.listArrow:hover {
	margin-left: 6px;
}

/*FOOTER*/
#footer {
	border-top: 5px solid #555;
	color: #666 !important;
}

#footer h3, #footer p, #footer a {
	font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-weight: normal !important;
	color: #eee !important;
}

#footer h3 {
	margin-top: 18px !important;
}

#footer h3, #footer p, .copyright {
	font-size: 14px !important;
	color: #666 !important;
}

#footer a:hover {
	color: red !important;
}

#footer a {
	color: #666 !important;
}

#footer .row {
	margin-bottom: 1px;
}

#subscribe-field {
/*width: 75% !important;
	background: #24221F;
	color: #9C927F;
	border-color: #595A59;*/
	width: 73% !important;
	height: auto;
	padding: 7px !important;
	border: 1px solid #666 !important;
	background: rgba(121,100,77,0.09);
	margin-bottom: 4px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
	font-size: 100% !important;
}

/*Slider*/
.sliderBorder {
	border: 20px solid #8f817c !important;
}

.btn-success, .btn a {
	color: #FFF !important;
}";s:10:"post_title";s:7:"safecss";s:12:"post_excerpt";s:3:"DSW";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:7:"safecss";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-04-11 19:57:42";s:17:"post_modified_gmt";s:19:"2014-04-11 19:57:42";s:21:"post_content_filtered";s:11635:"h1{font-size:140% !important}h2{font-size:100% !important}h3{font-size:95% !important}#footer h3{color:#ddd !important}strong,textBox strong{color:#888 !important;font-weight:400 !important}h3 a:hover{color:#666 !important}h3 a{color:#0080BC !important}.infoBox h1,.infoBox h2,.infoBox h3,.infoBox h4,.infoBox h5,.infoBox h6{text-transform:uppercase !important;color:#777 !important}.zn_post_image{width:auto !important}.gform_wrapper{max-width:100% !important}.swEmail input,.gform_wrapper select,.gform_wrapper textarea{font-size:100% !important;padding:5px!important;font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;color:#666 !important}.gform_wrapper textarea{width:98% !important}.gform_wrapper select{padding:3px!important}.gform_wrapper .gfield_required{display:none !important}.swEmail .gform_footer input[type="submit"]{font-size:1.1em !important;color:#777 !important;padding:3px 12px!important;text-transform:uppercase !important}.mySpacer{margin-bottom:16px !important}.flexslider{margin:0 0 15px!important}.hideMe{display:none}section#content{margin-top:30px !important}.showbiz .divide20{height:8px !important}#showbiz_news_1 .showbiz-title,#showbiz_news_1 a.showbiz-title,#showbiz_news_1 a.showbiz-title:visited{font-size:14px !important;line-height:14px !important}.infoBox{border:1px solid #fff;padding:16px!important;background:#ececec;background:rgba(236,227,212,0.21);margin-bottom:6px !important}.infoBox p{font-size:90% !important}.infoBox a{color:blue !important}.infoBox a:hover{text-decoration:underline !important}.infoBox ul li a{color:#666 !important}.infoBox .title a{color:#666 !important;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif !important;font-size:80% !important}.infoBox .text{font-family:"Helvetica Neue", Helvetica, Arial, sans-serif !important;font-size:70% !important}.infoBox .btn{color:#333 !important;text-decoration:none !important}.infoBox .btn:hover{color:#333 !important;text-decoration:none !important}.infoBox .btn-success{color:#FFF !important;text-decoration:none !important}.infoBox .btn-success:hover{color:#FFF !important;text-decoration:none !important}.infoBox:hover{background:#E9E9E9}.infoBox .widget{margin-bottom:5px !important}.textBox,.iconHolder,.infoBox{-webkit-border-radius:8px;-moz-border-radius:8px;border-radius:8px}.textBox{padding:12px!important;background:#f5f5f5 url('http://www.stylesandwood-group.co.uk/sw-cms/wp-content/uploads/striped-bg2.png') repeat}.textBox,.iconHolder{-webkit-box-shadow:1px 1px 2px rgba(75,75,75,0.07);-moz-box-shadow:1px 1px 2px rgba(75,75,75,0.07);box-shadow:1px 1px 2px rgba(75,75,75,0.07);border:1px solid #e5e5e5}.textBox h1,.textBox h2,.textBox h3,.textBox h4,.textBox h5,.textBox h6{margin-top:0 !important}.textBoxDark{padding:12px!important;margin-bottom:16px !important;-webkit-box-shadow:1px 1px 2px rgba(75,75,75,0.07);-moz-box-shadow:1px 1px 2px rgba(75,75,75,0.07);box-shadow:1px 1px 2px rgba(75,75,75,0.07);border:1px solid #e5e5e5;background:#f5f5f5 url('http://www.stylesandwood-group.co.uk/sw-cms/wp-content/uploads/TextBoxBGDrk1.png') repeat}.iconHolder{background:#fff;margin-bottom:16px !important;padding-left:16px;padding-top:16px}.pageIcons img{-webkit-transition:all .3s ease-in-out;-o-transition:all .3s ease-in-out;transition:all .3s ease-in-out}.pageIcons img:hover{margin-top:-7px !important;box-shadow:0 0 0 5px #666 inset}.wpb_separator{padding-top:10px !important;padding-bottom:10px !important}.sidebar .widget .title:after{border:none !important}#sidebar .text,.sidebarBox .text{font-size:.8em !important}#sidebar,.sidebarBox h3{font-size:1.1em !important}#sidebar,.sidebarBox{border:1px solid #fff !important;padding:16px!important;background:#eee !important;background:rgba(236,227,212,0.21) !important;margin-left:27px !important}.myfFix{margin-top:30px !important}.SWNavBar{margin-top:20px !important}#header{z-index:99999 !important}.ccc-inner h2{background:none !important;color:#666 !important}.ccc-outer{background:none repeat scroll 0 0 #FFF !important;border:1px solid #BBB !important;box-shadow:0 0 5px rgba(0,0,0,0.3) !important}.ccc-close{background:url('http://www.stylesandwood-group.co.uk/sw-cms/wp-content/uploads/close.jpg') !important;background-position:0 -17px !important;border:0 none !important;position:absolute !important;right:20px !important;text-indent:-999em !important;top:25px !important;width:17px !important;height:17px !important}.SPG{margin-top:10px;margin-bottom:10px;margin-right:20px}.spgBox{width:98%;height:auto;padding:6px 6px 12px 12px;border:1px solid #CCC;background:rgba(121,100,77,0.09);margin-bottom:10px;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif !important;font-size:100% !important}.spgBoxF{width:73%;height:auto;padding:0 0 4px 10px;border:1px solid #666;background:rgba(121,100,77,0.09);margin-bottom:4px;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif !important;font-size:100% !important}.spgBoxF,.spgBox{-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}.SPG h2{color:red;font-weight:700;font-size:.9em;margin:4px 0}.spgBox:hover{text-decoration:none !important;color:#FFF}.SWprice{font-size:110% !important;color:green;vertical-align:-10px}.SPTxt{position:relative;vertical-align:-10px;font-size:80%;color:#999}.SPG a,.SPG a:hover{text-decoration:none}.spgBox a:hover,.SPG a:hover,.SPTxt a:hover{text-decoration:none !important;color:#FFF !important}.seemore{display:none !important}.hg-portfolio-carousel h3 a{color:#666 !important;font-size:1.5em !important}.ls-container{margin-bottom:35px !important}.ls-container h2{font-size:130% !important}.projects h2{font-size:135% !important;color:#555 !important}.projects strong{color:#555 !important}.projects p{font-size:95% !important;color:#777}.myDownloads{color:#1d74d7 !important;padding-top:110px !important;margin-top:30px !important;background:url(http://dutcosw.com/sw/wp-content/uploads/downloads-2-icon.png) no-repeat;height:92px}.myDownloads p{color:#1d74d7 !important}.icon-search:before{content:"" !important}#megaMenu ul.megaMenu li.menu-item.ss-nav-menu-mega.mega-colgroup>ul>li{padding-left:1% !important;padding-right:1% !important}#megaMenu ul li.menu-item.ss-nav-menu-mega ul li.menu-item.ss-nav-menu-item-depth-1{padding-right:0 !important;padding-left:17px !important}#megaMenu ul.megaMenu > li.menu-item > a{padding:8px 15px!important}#megaMenu.active{background:#555 !important;color:#fff !important}nav#main_menu > ul > li.active > a,nav#main_menu > ul > li > a:hover,nav#main_menu > ul > li:hover > a,nav#main_menu > ul > li > a{margin:-6px 0 0;margin-left:1px !important;margin-right:1px !important}nav#main_menu > ul ul li a:hover{color:#666 !important}#menu-sw-sub a{font-size:1em !important;color:#333 !important;text-transform:uppercase !important}#main_menu a{font-size:75% !important;margin-left:0 !important;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif !important}#main_menu{text-transform:uppercase !important;float:left !important;clear:both !important;position:relative;top:-43px !important}nav#main_menu > ul > li.active > a,nav#main_menu > ul > li a:hover{color:#fff !important}#menu-sw-main{z-index:999999 !important}.zn_menu_trigger{background:#555 !important;position:relative !important;top:20px !important}.zn_menu_trigger a{color:#eee !important}.zn_menu_trigger a:before{border-color:#eee!important;content:'';position:absolute;top:37%;left:0;width:.75em;height:.125em;border-top:.3em double #666;border-bottom:.125em solid #666}.zn_menu_trigger a,.zn_menu_trigger a:hover{position:relative;padding-left:1.2em;color:#666;font-size:14px;font-weight:700}.wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header{background:none !important}.wpb_content_element .wpb_accordion_header a{padding:0!important;border:none !important}.mySplitterFix{margin-top:45px !important}:focus{outline:-webkit-focus-ring-color none !important}.wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header h2{color:#666 !important;margin-top:10px !important;margin-bottom:10px !important;margin-left:30px !important}.wpb_accordion .wpb_accordion_wrapper .ui-state-default .ui-icon,.wpb_accordion .wpb_accordion_wrapper .ui-state-active .ui-icon{left:0 !important}.zn_def_header_style{min-height:90px !important;background:#f5f5f5 !important}#page_header{height:90px !important;border-bottom:1px solid #ccc;-webkit-box-shadow:0 1px 10px 0 #666;box-shadow:0 1px 10px 0 #666;margin-bottom:15px !important;min-height:none !important}.wpb_row ul.wpb_thumbnails-fluid > [class*="vc_span"]{background:rgba(218,218,218,0.81)url('/sw-cms/wp-content/uploads/leather.png') repeat;padding-left:15px !important;padding-right:15px !important;border:solid 1px #999 !important;margin-right:12px !important;margin-bottom:18px !important;-moz-box-shadow:0 0 9px 0 #777 !important;-webkit-box-shadow:0 0 9px 0 #777;box-shadow:0 0 9px 0 #777}.wpb_row ul.wpb_thumbnails-fluid > [class*="vc_span"] h2{font-size:1.5em !important;line-height:120% !important}.wpb_row ul.wpb_thumbnails-fluid > [class*="vc_span"] h2 a{color:#666 !important}#logo{position:relative !important;float:right !important;clear:left !important;margin:0!important}#logo a img{margin-top:15px !important;max-width:none !important;width:172px !important;height:63px !important}#header.style3 #logo a{border:none !important;background:none !important;padding:0!important;background:transparent !important;-ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#F5F5F5,endColorstr=#F5F5F5);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#F5F5F5,endColorstr=#F5F5F5);zoom:1 !important;background:transparent !important;min-height:none !important;padding:0 !important;line-height:0 !important;text-align:right !important;height:none !important}#header.style3 #logo a:after{border:none !important}#header{position:absolute !important}#header #bar{display:none;position:absolute;z-index:1;top:40px;left:250px}.ls-noskin .ls-thumbnail{top:-40px !important;left:7px !important;float:left !important}#arLink p:first-of-type{margin-top:-20px}#arTxt{position:absolute;color:red;left:20px;top:325px;z-index:100000}#clickTxt{font-weight:700;position:relative;left:20px;top:350px;background:#151515;color:#888;padding:0 10px;z-index:100000}#clickTxt:hover{padding:0 10px;background:#222;color:#ccc}.listArrow{-webkit-transition:all .3s ease-in-out;-o-transition:all .3s ease-in-out;transition:all .3s ease-in-out;margin-top:7px;margin-bottom:0;background:url(/uploads/2012/06/arrow_box_icon1.gif) no-repeat;border-bottom:1px solid #DDD;border-top-width:1px;border-right-width:1px;height:22px;padding-left:20px;width:80%}.listArrow a:link{color:#777 !important}.listArrow a:hover{text-decoration:none;color:red !important}.listArrow a:visited{text-decoration:none;color:#CCC !important}.listArrow:hover{margin-left:6px}#footer{border-top:5px solid #555;color:#666 !important}#footer h3,#footer p,#footer a{font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;font-weight:400 !important;color:#eee !important}#footer h3{margin-top:18px !important}#footer h3,#footer p,.copyright{font-size:14px !important;color:#666 !important}#footer a:hover{color:red !important}#footer a{color:#666 !important}#footer .row{margin-bottom:1px}#subscribe-field{width:73% !important;height:auto;padding:7px!important;border:1px solid #666 !important;background:rgba(121,100,77,0.09);margin-bottom:4px;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif !important;font-size:100% !important}.sliderBorder{border:20px solid #8f817c !important}.btn-success,.btn a{color:#FFF !important}";s:11:"post_parent";s:1:"0";s:4:"guid";s:46:"http://46.32.253.31/~dutco/sw/?safecss=safecss";s:10:"menu_order";s:1:"0";s:9:"post_type";s:7:"safecss";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}