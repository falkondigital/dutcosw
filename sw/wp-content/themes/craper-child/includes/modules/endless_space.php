<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<article class="parallex-section" style="background-image:url(<?php echo wp_get_attachment_url( $bg_img, 'full' ); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="display">
					<figure class="animated out" data-animation="fadeInLeft" data-delay="0"> <?php echo wp_get_attachment_image( $img, 'full' ); ?> </figure>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 animated out" data-animation="fadeInRight" data-delay="300">
				<h1><?php echo $title;?></h1>
				<p><?php echo $text;?></p>
				<a href="<?php echo $btn_link;?>" class="btn btn-transparent"><?php echo $btn_text;?></a> </div>
		</div>
	</div>
</article>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>