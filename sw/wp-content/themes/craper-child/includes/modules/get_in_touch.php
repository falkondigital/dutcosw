<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

					
<div class="comments">
	<h4><?php echo $title;?></h4>
	<p><?php echo $text;?></p>
	<form class="contact-form" method="post">
		<ul class="list-unstyled">
			<li class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<label><?php _e('Name', SH_NAME); ?></label>
					<input type="text" placeholder="<?php _e('enter a name', SH_NAME); ?>" class="input-block-level" name="contact_name" required="required">
					<label><?php _e('Email', SH_NAME); ?></label>
					<input type="email" placeholder="<?php _e('entery your email', SH_NAME); ?>" class="input-block-level" required="required" name="contact_email">
					<label><?php _e('Subject', SH_NAME); ?></label>
					<input type="text" placeholder="<?php _e('enter a subject', SH_NAME); ?>" class="input-block-level" name="contact_subject">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<label><?php _e('Message', SH_NAME); ?></label>
					<textarea placeholder="<?php _e('type a message', SH_NAME); ?>" class="input-block-level" required="required" name="contact_message"></textarea>
					<input type="submit" value="<?php _e('Send', SH_NAME); ?>" class="btn">
				</div>
			</li>
		</ul>
	</form>
</div>

<?php 

$output = ob_get_contents();
ob_end_clean();

?>