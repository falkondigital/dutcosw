<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<div class="pricing-table">
	<ul>
		<li class="table-heading"><h5><?php echo $title;?></h5> <span><em><?php echo $price;?></em>/month</span></li>
		
		<?php $list = explode("\n",$content);
			foreach ($list as $value):
		?>
		
		<li><?php echo $value;?></li>
		
		<?php endforeach; ?>
		
		<li class="table-btm"><a href="<?php echo $btn_link;?>" class="btn btn-theme"><?php echo $btn;?></a></li>
	</ul>
</div>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>