<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<article class="parallex-section" style="background-image:url(<?php echo wp_get_attachment_url( $bg_img, 'full' ); ?>);">
	<div class="container">
		<div class="align-center">
			<div class="iconic iconic-large animated out" data-animation="fadeInUp" data-delay="0"> <a href="<?php echo $icon_link;?>" class="icon-fa icon-play-circle icon-5x"></a> </div>
			<h1 class="animated out" data-animation="fadeInUp" data-delay="0"><?php echo $title;?></h1>
			<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
				<h2><?php echo $sub_title;?></h2>
			</header>
			<a class="btn btn-transparent" href="<?php echo $btn_link;?>"><?php echo $btn_text;?></a> </div>
	</div>
</article>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>