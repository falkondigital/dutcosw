	<h4>Project Description</h4>
	<p><?php the_excerpt();?></p>
	<h4>Features</h4>
	<ul class="bullet-2">
	
		<?php $features = sh_set($meta, 'feature_group');

		$delay = 0;
		
		foreach($features as $key => $value):?>
					
			<li class="animated out" data-animation="fadeInLeft" data-delay="<?php echo $delay;?>"><?php echo sh_set( $value, 'feature' ); ?></li>
		
			<?php $delay += 200; 
		endforeach; ?>
		
	</ul>
	<h4>Client</h4>
	<p><?php echo sh_set($meta, 'client_name');?> <br>
		website <a href="<?php echo sh_set($meta, 'project_url');?>"><?php echo sh_set($meta, 'project_url');?></a></p>
