<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<article>
	<div class="section">
		<div class="container">
			<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
				<h2><?php echo $title;?></h2>
			</header>
			<div class="row">
		
				<?php while( have_posts()): the_post(); 
							$meta = get_post_meta( get_the_id(), '_sh_services_meta', true );//printr($meta);?>
		
				<div class="col-xs-12 col-sm-3 col-md-3 animated out" data-animation="bounceIn" data-delay="<?php echo $delay;?>">
					<div class="feature-box">
						<div class="feature-up">
							<div class="iconic  theme-color"> <i class="icon-fa <?php echo sh_set($meta, 'font_awesome_icon');?> icon-7x"></i> </div>
							<p><?php the_title();?></p>
						</div>
						<div class="feature-down">
							<h4><?php the_title();?></h4>
							<p><?php echo $this->excerpt(get_the_excerpt(), 90); ?></p>
						</div>
					</div>
					<!--/feature-box --> 
					
				</div>
				
				<?php $delay+=200; endwhile; ?>
			
			</div>
		</div>
	</div>
</article>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>