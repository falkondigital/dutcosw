<?php  get_header(); ?>
<article class="banner">
	<div class="container">
		
		<h2><?php the_title(); ?></h2>
		
		<div class="breadcrumbs">
			<ul class="breadcrumb">
			<?php //if( get_option( 'show_breadcrumbs') )
				echo get_the_breadcrumb(); ?>
			</ul>
		</div>
		
	</div>
</article>

<article role="main">
	
	<?php while (have_posts()) : the_post(); 
			$meta = get_post_meta( get_the_id(), '_sh_team_meta', true );//printr($meta);?>


	<div class="section no-padding-bottom">
		<div class="container">
			<div class="author-box">
				<div class="author-col">
					<figure class="image"><?php the_post_thumbnail('270x237');?></figure>
					<ul class="options">
						<li><a href="<?php echo sh_set($meta, 'hireme_link');?>" class="btn btn-primary">Hire Me</a></li>
						
						<?php $social_icon = sh_set($meta, 'social_icon_group');

									
						foreach($social_icon as $key => $value):
							
							$icon = str_replace( 'fa-', 'icon-', sh_set( $value, 'social_icon' ) ); ?>
						
							<li><a href="<?php echo sh_set( $value, 'social_url' ); ?>" class="icon-fa <?php echo $icon; ?>"></a></li>
						
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="author-detail">
					<h2><?php echo sh_set($meta, 'about_title');?></h2>
					<h5><?php echo sh_set($meta, 'about_subtitle');?></h5>
					<p><?php echo sh_set($meta, 'about_text');?></p>
				</div>
			</div>
			<!-- /author-box --> 
		</div>
	</div>
	<!-- /section -->
	
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h2>Some of my Skills</h2>
					<p class="margin-btm-50"><?php echo sh_set( $meta, 'skill_text' ); ?></p>
					<ul class="unstyled">
						<li>Webdesign Art Studios</li>
						<li><strong>website</strong> <a href="<?php echo sh_set( $meta, 'site_url' ); ?>"><?php echo sh_set( $meta, 'site_url' ); ?></a></li>
						<li><strong>themes</strong> <a href="<?php echo sh_set( $meta, 'theme_url' ); ?>"><?php echo sh_set( $meta, 'theme_url' ); ?></a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="progress-bars">
					
						<?php $skills = sh_set($meta, 'skills_group');

									
									foreach($skills as $key => $value):?>
						
							<div class="progress-bar">
								<p><?php echo sh_set( $value, 'title' ); ?></p>
								<div class="progress">
									<div class="bar animated" data-cents="<?php echo sh_set( $value, 'skill_percent' ); ?>"> <span></span> </div>
								</div>
							</div>
							<!-- /progress-bar -->
							
						<?php endforeach; ?>
						
					</div>
					<!-- /progress-bars --> 
				</div>
			</div>
		</div>
	</div>
	<!-- /section -->
	
	<?php the_content();?>
	
	<?php endwhile;?>
	
</article>
<?php get_footer(); 