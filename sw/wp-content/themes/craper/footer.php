<footer>
        <div class="container">
            <div class="row">
			
			<div class="col-md-4">
			<h3>Styles&amp;Wood Group Links</h3>			
				<p><a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br />
				<a href="http://www.stylesandwood-group.co.uk/sitemap/">Sitemap</a><br />
				<a href="http://www.stylesandwood-group.co.uk/contact/">Contact Us</a></p>
			</div>
	
			<?php dynamic_sidebar('footer-sidebar'); ?>

		<div class="col-md-4">
		<h3>Current Share Price</h3>		
			 <p><?php include('stock2014.php'); ?> GBp</p>
		</div>
		</div>
        
		<div class="row">
				<div class="span12">
					<div class="bottom fixclear">
					
					<ul class="social-icons colored fixclear"><li class="title">See more from the Styles&amp;Wood Group</li><li class="social-twitter"><a target="_blank" href="https://twitter.com/StylesandWood">Twitter</a></li><li class="social-linkedin"><a target="_blank" href="http://www.linkedin.com/company/78033">LinkedIn</a></li></ul>					
											
						<div class="copyright">
							
							
							<a href="http://www.stylesandwood-group.co.uk"><img alt="Styles&amp;Wood Group PLC" src="http://stylesandwood-group.co.uk/sw-cms/wp-content/uploads/SylesWood-sm.png"></a><p>&copy; 2016 Copyright - Styles&amp;Wood Group PLC</p>	        					
							
						</div><!-- end copyright -->
							
								  		

					</div><!-- end bottom -->
				</div>
			</div>
		
		</div>
    </footer>
</div>
<!-- /pageWrapper -->

<?php wp_footer(); ?>

<!-- Don't forget analytics -->
	
</body>

</html>