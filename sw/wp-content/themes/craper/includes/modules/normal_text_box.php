<?php $t = $GLOBALS['_sh_base'];
ob_start();

?>

<?php $delay = 0;?>

<div class="section no-padding-bottom">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title;?></h2>
		</header>
		<div class="row">

			<p><?php echo $text;?></p>
		
		</div>
	</div>
</div>

<?php 

$output = ob_get_contents();
ob_end_clean();

?>