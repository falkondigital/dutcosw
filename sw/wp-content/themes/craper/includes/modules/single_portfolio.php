<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php if( have_posts()):?>
<div class="section">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title;?></h2>
		</header>
		
		<?php while( have_posts()): the_post();
					$meta = get_post_meta( get_the_id(), '_sh_portfolio_meta', true );//printr($meta);?>
		
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8">
				<div class="display animated out" data-animation="fadeInUpBig" data-delay="0">
					<figure class="image-caption"> <?php the_post_thumbnail('770x600');?>
						<figcaption>
							<h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
							<p><?php echo get_the_term_list(get_the_id(), 'portfolio_category', '', ', '); ?></p>
						</figcaption>
					</figure>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4">
				<h4>Project Description</h4>
				<p><?php echo $this->excerpt(get_the_excerpt(), 180); ?></p>
				<h4>Features</h4>
				<ul class="bullet-2">
				
					<?php $features = sh_set($meta, 'feature_group');

									$delay = 0;
									foreach($features as $key => $value):?>

					<li class="animated out" data-animation="fadeInLeft" data-delay="<?php echo $delay;?>"><?php echo sh_set( $value, 'feature' ); ?></li>
					
					<?php $delay+=200; endforeach; ?>
					
				</ul>
				<h4>Client</h4>
				<p><?php echo sh_set($meta, 'client_name');?> <br>
					website <a href="<?php echo sh_set($meta, 'project_url');?>"><?php echo sh_set($meta, 'project_url');?></a></p>
			</div>
		</div>
		
		<?php endwhile; ?>
		
	</div>
</div>


<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>