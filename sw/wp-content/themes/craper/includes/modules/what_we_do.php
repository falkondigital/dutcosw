<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title; ?></h2>
		</header>
		<div class="row">

			<?php while( have_posts()): the_post(); 
							$meta = get_post_meta( get_the_id(), '_sh_services_meta', true );//printr($meta);?>

			<div class="col-xs-12 col-sm-3 col-md-3 align-center animated out" data-animation="fadeInUpBig" data-delay="<?php echo $delay;?>">
				<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
				<div class="circular-progress">
					<input class="knob" data-width="100%" data-min="0" data-max="100"  data-bgColor="#e1e1e1" data-fgColor="#22c0e8" data-thickness=".05" data-readOnly="true" value="<?php echo sh_set($meta, 'skill_percent');?>">
				</div>
				<!-- circular-progress -->
				<p><?php echo $this->excerpt(get_the_excerpt(), 160); ?></p>
			</div>
			
			<?php $delay+=200; endwhile; ?>
			
		</div>
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>