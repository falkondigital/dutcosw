<?php

return array(
	'title' => __('Craper Theme Options', SH_NAME),
	'logo' => get_template_directory_uri().'/images/logo.png',
	'menus' => array(
		
		/** General Settings */
		array(
				'title' => __('General Settings', SH_NAME),
				'name' => 'general_settings',
				'icon' => 'font-awesome:icon-cogs',
				'controls' => array(
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('General settings', SH_NAME),
							'name' => 'general',
							'description' => __('This section contains general options about the theme.', SH_NAME),
							'fields' => array(
								array(
									'type' => 'color',
									'name' => 'color_scheme',
									'label' => __('Color Scheme', SH_NAME),
									'description' => __('Select the color you want to apply to the whole website.', SH_NAME),
									'default' => '#EC644B',
								),
			             	),
						),
						array(
						'type' => 'section',
						'title' => __('Header settings', SH_NAME),
						'name' => 'header_settings',
						'description' => __('This section contains options about the header', SH_NAME),
						'fields' => array(
							array(
								'type' => 'textbox',
								'name' => 'header_phone',
								'label' => __('Header Phone Number', SH_NAME),
								'description' => __('Enter the phone number to show in header section', SH_NAME),
								'default' => '',
							),
							array(
								'type' => 'textbox',
								'name' => 'header_email',
								'label' => __('Header Eamil', SH_NAME),
								'description' => __('Enter the email to show in header section', SH_NAME),
								'default' => '',
							),
							array(
								'type' => 'upload',
								'name' => 'favicon',
								'label' => __('Favicon', SH_NAME),
								'description' => __('Upload the favicon, should be 16x16', SH_NAME),
								'default' => '',
							),
							array(
								'type' => 'upload',
								'name' => 'logo',
								'label' => __('Logo', SH_NAME),
								'description' => __('Upload the logo', SH_NAME),
								'default' => '',
							),
						),
					),
					array(
							 'type' => 'section',
							 'title' => __('Twitter Settings', SH_NAME),
							 'name' => 'twitter_settings',
							 'description' => __('This section contain the information about twitter api settings', SH_NAME),
							 'fields' => array(
							  		array(
							   			'type' => 'textbox',
							   			'name' => 'api',
							   			'label' => __('API Key', SH_NAME),
							   			'description' => __('Enter the twitter API key, You can get the api at http://developer.twitter.com', SH_NAME),
							   			'default' => '',
							  			),
							  		array(
							   			'type' => 'textbox',
							   			'name' => 'api_secret',
							   			'label' => __('API Secret', SH_NAME),
							   			'description' => __('Enter the API secret', SH_NAME),
							   			'default' => '',
							  			),
							  		array(
							   			'type' => 'textbox',
							   			'name' => 'token',
							   			'label' => __('Token', SH_NAME),
							   			'description' => __('Enter the twitter api token', SH_NAME),
							   			'default' => '',
							  			),
							  		array(
							   			'type' => 'textbox',
							   			'name' => 'token_secret',
							   			'label' => __('Token Secret', SH_NAME),
							   			'description' => __('Enter the API token secret', SH_NAME),
							   			'default' => '',
							  ),
							  
							 ),
							),
				)
			),
		
		/** Sidebar Creator Settings */
		array(
				'title' => __('Sidebar Creator', SH_NAME),
				'name' => 'sidebar_creater_main',
				'icon' => 'font-awesome:icon-th-large',
				'controls' => array(
                       	array(
							  'type' => 'builder',
							  'repeating' => true,
							  'sortable'  => true,
							  'label' => __('Sidebar', SH_NAME),
							  'name' => 'sidebar_creator',
							  'description' => __('You can create as many sidebars as you required.', SH_NAME),
							  'fields' => array(
								 array(
										'type' => 'textbox',
										'name' => 'sidebar',
										'label' => __('Sidebar Name:', SH_NAME),
										
										'default' => '',
									   ),
							  ),
							 ),
				)
		),
			
		/** Sidebar Settings*/
		array(
				'title' => __('Sidebar Settings', SH_NAME),
				'name' => 'sidebar_settings',
				'icon' => 'font-awesome:icon-th-list',
				'controls' => array(
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('404 Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section1',
							'description' => __('This section contains 404 sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => '404_sidebar_layout',
								'label' => __('404 Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => '404_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Category Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section2',
							'description' => __('This section contains category sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'category_sidebar_layout',
								'label' => __('Category Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'category_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Archive Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section3',
							'description' => __('This section contains Archive Sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'archive_sidebar_layout',
								'label' => __('Archive Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'archive_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Author Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section4',
							'description' => __('This section contains Author Sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'author_sidebar_layout',
								'label' => __('Author Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'author_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Search Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section5',
							'description' => __('This section contains Search Sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'search_sidebar_layout',
								'label' => __('Search Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'search_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Tag Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section6',
							'description' => __('This section contains Tag Sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'tag_sidebar_layout',
								'label' => __('Tag Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'tag_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
												array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Single post page sidebar', SH_NAME),
							'name' => 'sidebar_setting_section7',
							'description' => __('This section contains Single post page Sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'single_post_sidebar_layout',
								'label' => __('Product Category Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'single_post_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
						array(
							'type' => 'section',
							'repeating' => true,
							'sortable'  => true,
							'title' => __('Product Category Sidebar Settings', SH_NAME),
							'name' => 'sidebar_setting_section8',
							'description' => __('This section contains Product Category Sidebar settings', SH_NAME),
							'fields' => array(
								array(
								'type' => 'radioimage',
								'name' => 'product_category_sidebar_layout',
								'label' => __('Product Category Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
										),
										array(
											'type' => 'select',
											'name' => 'product_category_sidebar',
											'label' => __('Sidebar', SH_NAME),
											'description' => __('Choose an sidebar for this', SH_NAME),
											'default' => '',
											'items' => sh_get_sidebars(true)	
										),

							),
						),
				)
			),
	
		/** 404 page settingss Settings */
		array(

				'title' => __('404 Page Settings', SH_NAME),

				'name' => '404_page_settings',

				'icon' => 'font-awesome:icon-exclamation-sign',

				'controls' => array(

						array(

							'type' => 'textbox',
							'name' => '404_title',
							'label' => __('Title', SH_NAME),
							
							'default' => __('404 Not Found', SH_NAME),
						),
						array(
							'type' => 'textarea',
							'name' => '404_description',
							'label' => __('404 Message', SH_NAME),
							
							'default' => '',
	
						),
						array(
							'type' => 'upload',
							'name' => '404_image',
							'label' => __('404 Image', SH_NAME),
							
							'default' => '',
	
						),
						array(
							'type' => 'toggle',
							'name' => '404_home_button',
							'label' => __('Show Homepage Link', SH_NAME),
							'default' => 1,
	
						),
				)
			),			
		
		/** Social Network Settings */
		array(
				'title' => __('Socializing Settings', SH_NAME),
				'name' => 'socializing_settings',
				'icon' => 'font-awesome:icon-share-sign',
				'controls' => array(
					
					array(
						'type' => 'builder',
						'repeating' => true,
						'sortable'  => true,
						'label' => __('Social Icons', SH_NAME),
						'name' => 'social_icons',
						'description' => __('This section is used for theme color settings', SH_NAME),
						'fields' => array(
							array(
								'type' => 'select',
								'name' => 'icon',
								'label' => __('Social Icon', SH_NAME),
								
								'default' => 'facebook',
								'items' => array(
									'data' => array(
										array(
											'source' => 'function',
											'value' => 'vp_get_social_medias',
										),
									),
								),
							),
							array(
								'type' => 'textbox',
								'name' => 'title',
								'label' => __('Title', SH_NAME),
								
								'default' => '',
							),
							array(
								'type' => 'textbox',
								'name' => 'link',
								'label' => __('URL', SH_NAME),
								
								'default' => '',
							),
							
						),
					),
					
					
				)
			),
			
		/** Blog page Settings*/
		array(
				'title' => __('Blog page Settings', SH_NAME),
				'name' => 'blog_page_settings',
				'icon' => 'font-awesome:icon-archive',
				'controls' => array(
							array(
								'type' => 'section',
								'title' => __('Blog page Settings', SH_NAME),
								'name' => 'blog_page_setting_section',
								'description' => __('Set the sidebar for each template page', SH_NAME),
								'fields' => array(
									 	array(
											'type' => 'radioimage',
											'name' => 'blog_page_view_list',
											'label' => __('Blog View', SH_NAME),
											'description' => __('Choose the blog view', SH_NAME),
											'items' => array(
													array(
														'value' => 'grid',
														'label' => __('Blog with Left Sidebar', SH_NAME),
														'img' => get_template_directory_uri().'/images/grid-view.png',
														),
													array(
														'value' => 'list',
														'label' => __('Blog with Right Sidebar', SH_NAME),
														'img' => get_template_directory_uri().'/images/list-view.png',
														),
											),
							),
							 array(
								'type' => 'radioimage',
								'name' => 'blog_layout',
								'label' => __('Blog page Sidebar', SH_NAME),
								'description' => '',
								'items' => array(
											 array(
												'value' => 'full',
												'label' => __('Full Width', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
												),
	
											 array(
												'value' => 'left',
												'label' => __('Left Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
												),
											 array(
												'value' => 'right',
												'label' => __('Right Sidebar', SH_NAME),
												'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
												),
			 
											),
							),
							 array(
								'type' => 'select',
								'name' => 'blog_sidebar',
								'label' => __('Sidebar', SH_NAME),
								'description' => __('Choose an sidebar for this deal', SH_NAME),
								'default' => '',
								'items' => sh_get_sidebars(true)	
							),
				        ),
					),
				)
			),
			
						
	)
);



/**
 *EOF
 */