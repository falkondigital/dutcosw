<?php

function wpstrapslider_customize_register($wp_customize) {
    // Setting group for selecting slider
   $wp_customize->add_section( 'wpstrapslider_general_options' , array(
    'title'      => __('General Options','wpstrapslider'),
    'priority'   => 30,
   ) );
   
   $wp_customize->add_section( 'wpstrapslider_slider_options' , array(
    'title'      => __('Slider Options','wpstrapslider'),
    'priority'   => 35,
   ) );
   
   $wp_customize->add_section( 'wpstrapslider_footer_options' , array(
    'title'      => __('Footer Options','wpstrapslider'),
    'priority'   => 37,
   ) );

/**
 * Lets begin adding our own settings and controls for this theme
 * Plus organize it in sequence in each setting group with a priority level
 */
	
	$wp_customize->add_setting(
    'wpstrapslider_alternative_navbar'
    );

    $wp_customize->add_control(
    'wpstrapslider_alternative_navbar',
    array(
        'type' => 'checkbox',
        'label' => __('Switch to alternative navbar? Adds site description','wpstrapslider'),
        'section' => 'wpstrapslider_general_options',
		'priority' => '1',
        )
    );
	
	$wp_customize->add_setting(
    'wpstrapslider_blogfeed_excerpts'
    );

    $wp_customize->add_control(
    'wpstrapslider_blogfeed_excerpts',
    array(
        'type' => 'checkbox',
        'label' => __('Switch to exceprts on blog feed?','wpstrapslider'),
        'section' => 'wpstrapslider_general_options',
		'priority' => '2',
        )
    );
	
	$wp_customize->add_setting(
    'wpstrapslider_excerpt_length'
    );

    $wp_customize->add_control(
    'wpstrapslider_excerpt_length',
    array(
        'type' => 'text',
		'default' => '',
        'label' => __('Define the excerpt length (default is 80 chars)','wpstrapslider'),
        'section' => 'wpstrapslider_general_options',
		'priority' => '3',
        )
    );
	
	$wp_customize->add_setting(
    'wpstrapslider_attachment_commentform_visibility'
    );

    $wp_customize->add_control(
    'wpstrapslider_attachment_commentform_visibility',
    array(
        'type' => 'checkbox',
        'label' => __('Hide Comment Form on the Attachment page','wpstrapslider'),
        'section' => 'wpstrapslider_general_options',
		'priority' => '4',
        )
    );
	
	// Begin slider section
	$wp_customize->add_setting( 'wpstrapslider_slider_transition', array(
		'default' => 'slide',
	) );

	
	$wp_customize->add_control( 'wpstrapslider_slider_transition', array(
    'label'   => __( 'Slider Transition', 'wpstrapslider' ),
    'section' => 'wpstrapslider_slider_options',
	'priority' => '2',
    'type'    => 'radio',
        'choices' => array(
            'slide' => __( 'Slide', 'wpstrapslider' ),
            'slide carousel-fade' => __( 'Fade', 'wpstrapslider' ),
        ),
    ));
	
    //  = Category Dropdown =

    $categories = get_categories();
	$cats = array();
	$i = 0;
	foreach($categories as $category){
		if($i==0){
			$default = $category->slug;
			$i++;
		}
		$cats[$category->slug] = $category->name;
	}
 
	$wp_customize->add_setting('wpstrapslider_slide_cat', array(
		'default'        => $default
	));
	$wp_customize->add_control( 'wpstrapslider_slide_cat', array(
		'settings' => 'wpstrapslider_slide_cat',
		'label'   => __('Select Slider Category:','wpstrapslider'),
		'section'  => 'wpstrapslider_slider_options',
		'priority' => '3',
		'type'    => 'select',
		'choices' => $cats,
	));
	
	$wp_customize->add_setting(
    'wpstrapslider_slide_number'
    );

    $wp_customize->add_control(
    'wpstrapslider_slide_number',
    array(
        'type' => 'text',
		'default' => 5,
        'label' => __('Number Of Slides To Show - i.e 10 (default is 5)','wpstrapslider'),
        'section' => 'wpstrapslider_slider_options',
		'priority' => '4',
        )
    );
	
	$wp_customize->add_setting(
    'wpstrapslider_slider_excerpt'
    );

    $wp_customize->add_control(
    'wpstrapslider_slider_excerpt',
    array(
        'type' => 'text',
		'default' => 40,
        'label' => __('Enter excerpt length for the slider (default is 40)','wpstrapslider'),
        'section' => 'wpstrapslider_slider_options',
        )
    );
	
	$wp_customize->add_setting(
    'wpstrapslider_slider_visibility'
    );

    $wp_customize->add_control(
    'wpstrapslider_slider_visibility',
    array(
        'type' => 'checkbox',
        'label' => __('Show Home Slider','wpstrapslider'),
        'section' => 'wpstrapslider_slider_options',
		'priority' => 1,
        )
    );
	
	// Begin footer section
	$wp_customize->add_setting(
    'wpstrapslider_copyright_textbox',
    array(
        'default' => __('Copyright &copy; 2013','wpstrapslider'),
    ));
	
	$wp_customize->add_control(
    'wpstrapslider_copyright_textbox',
    array(
        'label' => __('Copyright Text','wpstrapslider'),
        'section' => 'wpstrapslider_footer_options',
        'type' => 'text',
    ));
	
	$wp_customize->add_setting(
    'wpstrapslider_credits_visibility'
    );

    $wp_customize->add_control(
    'wpstrapslider_credits_visibility',
    array(
        'type' => 'checkbox',
        'label' => __('Hide Footer Credits - We understand if you must!','wpstrapslider'),
        'section' => 'wpstrapslider_footer_options',
        )
    );
}

add_action( 'customize_register', 'wpstrapslider_customize_register' );